# Changelog

## Vorläufiger Endstand
- Allgemein
  - Abstract an unmittelbaren Anfang der Arbeit verschoben
- Einleitung
- Implementierung einer Webcomponent
  - Durchführung
    - Einschränkung IE hinzugefügt
  - Auswertung
    - Verweis auf erfüllte Anforderungen
    - Für Tests verwendete Browser hinzugefügt
- Ausblick
- Anhang
  - erfüllte Anforderungen

## Zwischenstand 3 (22.06.)

- Allgemein
  - Rechtschreib- und Grammatikfehler korrigiert
  - Angemerkte Satzkonstruktionen überarbeitet
  - Inhaltliche Korrekturen nach Andreas durchsicht
- Frameworks zur Entwicklung von Webcomponents
  - Stencil-Builds hinzugefügt
- Implementierung einer Webcomponent
  - Vorbereitung
    - Test-Server hinzugefügt
  - Durchführung in erster Iteration für alle Frameworks beschrieben
    - Optimierung beschrieben
  - Auswertung
- Anhang

## Zwischenstand 2.1 (11.06.)

- Allgemein
  - Rechtschreib- und Grammatikfehler korrigiert
  - Angemerkte Satzkonstruktionen überarbeitet
- Grundlagen
  - Verhalten von Slots ohne Namen ergänzt.
- Frameworks zur Entwicklung von Webcomponents
  - Beschreibung der Frameworks
  - Vergleich der Frameworks
- Implementierung einer Webcomponent
  - Vorbereitung
    - Boolean-Parameter mit Präfix is*
  - Durchführung in erster Iteration beschrieben

## Zwischenstand 2 (01.06.)

- Allgemein
  - Rechtschreib- und Grammatikfehler korrigiert
  - Angemerkte Satzkonstruktionen überarbeitet
  - Akademischer Grad von Jochen hinzugefügt
  - Fehler der Fußnotenlinks behoben
- Grundlagen
  - Ursprung des Component-based Software Engineering hinzugefügt
  - Abgrenzung Frameworks und Services
  - Fußnote zur Abb. Stufen der Entkopplung in Beschriftung eingefügt
  - Komponentenkategorien jetzt auf Deutsch
  - Webcomponents-Standards nur noch mit englischer Bezeichnung
  - Herkunft der Webcomponents-Standards ergänzt
  - Vergleich Webcomponents und Komponentendefinition
- Webcomponents vs. Framework-Komponenten
  - Beschreibung der Frameworks
  - Vergleich mit Webcomponents
  - Zwischenfazit
- Frameworks zur Entwicklungvon Webcomponents
  - Zweck
  - Wahl der Frameworks
- Implementierung einer Webcomponent
  - Vorbereitung
    - Zweck des LSG hinzugefügt
    - "Bilder in Kopfzeile" aus Anforderung entfernt
    - "Quellcode in Englisch" als sonstige Anforderung hinzugefügt
    - Entwurf
    - Entwicklungsumgebung (noch unvollständig)
  - Durchführung (noch unvollständig)
- Erstellung von Abbildungen
  - Überarbeitung "Zerlegung und Komposition einer Anwendung"
  - Komponenten des Accordions

## Zwischenstand 1 (11.05.)

 - Dokumentenstruktur
 - Grundlagen
   - Komponenten in der Softwareentwicklung
   - Komponentenorientierte Webentwicklung
   - Webcomponents
 - Implementierung einer Webcomponent
   - Beschreibung der zu implementierenden Komponente
   - Anforderungen an die zu implementierende Komponente
 - Erstellung von Abbildungen
   - Zerlegung und Komposition einer Anwendung
   - Schematische Darstellung eines Accordions
