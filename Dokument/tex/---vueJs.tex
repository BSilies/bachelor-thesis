\subsection{Vue.js}
Vue.js, im Folgenden Vue, wurde in seiner ersten Version im Jahr 2014 von Evan You, einem ehemaligen
Google-Entwickler veröffentlicht und ist somit das jüngste der beschriebenen
Frameworks\footnote{Dies gilt unter der Voraussetzung, dass Angular JS und Angular ab Version 2
trotz weitreichender Veränderungen als ein Framework betrachtet werden.}.
\cite[]{martinHackernoonAngularReactVue} Vue beschreibt sich selbst als einfach zu erlernendes
Framework, da Kenntnisse in \ac{html}, \ac{css} und JavaScript ausreichend sind, um Anwendungen mit
Vue entwickeln zu können. Darüber hinaus bietet Vue die Möglichkeit auch nur Teile einer Anwendung
mittels des Frameworks zu entwickeln. \cite[]{vueHome}

Die Ähnlichkeit zu Angular, die im weiteren Verlauf noch beschrieben wird, ist in dem Hintergrund
des Gründers von Vue begründet. Evan You war Entwickler bei Google und dort insbesondere mit
Prototyping von Webprojekten beschäftigt. Einige dieser Projekte nutzten nach seiner Aussage
Angular, dessen Grundprinzipien ihm gut gefielen, aber das Framework seiner Meinung nach zu groß und
schwer war. Aus diesem Grund begann er mit der Entwicklung von Vue, mit dem Ziel ein Framework zu
schaffen, das deutlich leichter als Angular ist, jedoch die Grundkonzepte, wie zum Beispiel
Data-Binding, ebenfalls umsetzt. \cite[]{cromwellFreeCodeCampInterviewVue}

Die Basis einer jeden Vue-Applikation ist die sogenannte Vue-Instanz mit der Vue-Funktion. Diese
Funktion enthält ein Options-Objekt, welches genutzt wird, um das Verhalten der Anwendung zu
definieren. Eine solche Beispiel-Instanz zeigt Code-Beispiel \ref{code:vueVueInstanz}. Eine dieser
Optionen ist das Daten-Objekt \inlinecode{data}, dessen Inhalt in dem Reactivity System (mögliche
Übersetzung: Reaktivitäts-System) von Vue registriert wird und somit bei Änderungen eine
Aktualisierung der Views mit sich bringt. Auch in Vue sind Views, analog zu Angular, darstellbare
Elemente inklusive ihres Verhaltens. Mittels des Elements \inlinecode{el} in dem Options-Objekt wird
darüber hinaus festgelegt an welches \ac{dom}-Element die Vue-Instanz gebunden wird. In der Regel
wird hier eine Id angegeben, um ein Element eindeutig zu identifizieren. Ebenso wie
\inlinecode{data} und \inlinecode{el} sind auch die \glspl{lifecycleCallback} teil des
Options-Objekts, auf die jedoch nicht weiter eingegangen werden soll, da sie auch in Vue den
gleichen Zweck haben wie in React oder Angular. \cite[]{vueVueInstance}

\begin{lstlisting}[float, caption=Beispiel Vue-Instanz, label=code:vueVueInstanz]
new Vue({
	el: '#app',
	data: {
		a: 1
	},
	created: function () {
		console.log('a is: ' + this.a)
	}
})
\end{lstlisting}

Weitere Elemente des Options-Objekts können berechnete Werte in dem Element \inlinecode{computed},
sowie Funktionen im \inlinecode{methods}-Element sein. Beide Elemente können dazu dienen im
\inlinecode{data}-Element definierte Daten zu nutzen um neue Daten zu berechnen. Das
\inlinecode{computed}-Element bietet jedoch den Vorteil, dass diese zu berechnenden Werte bei
Änderung der Basiswerte berechnet und dann gecached werden und somit einen Performance-Vorteil
gegenüber der \inlinecode{methods} bietet, die nur bei Aufruf ausgeführt werden und diesen Wert
jedes Mal neu berechnen würden. Funktionen innerhalb des \inlinecode{methods}-Elements sollten in
erster Linie für \gls{event}-Handling und ähnliches Verhalten genutzt werden; nicht aber als
Datenlieferant. Neben diesen Elementen existiert weiterhin das \inlinecode{watcher}-Element, welches
ebenso wie die unter \inlinecode{computed} berechneten Werte auf Änderungen von definierten Werten
reagieren kann. Watcher werden in der Regel dort eingesetzt wo Asynchronität gefordert ist oder die
Berechnung von Werten zeitaufwändig ist. Darüber hinaus können in einem Watcher mehrere Aktionen
gebündelt werden, was im \inlinecode{computed}-Kontext jedoch nicht möglich ist.
\cite[]{vueComputedWatchers}

Zur Ausgestaltung der Views bedient sich Vue einer auf \ac{html} basierenden Template-Syntax, die
jedoch nicht verpflichtend ist, da auch rein JavaScript genutzt werden kann um Render-Funktionen
direkt zu schreiben. Hier soll jedoch nur die Template-Syntax betrachtet werden. Ebenso wie in
Angular können Inhalte per Interpolation in das Template eingebracht werden. Auch hier wird die
Syntax der doppelten geschweiften Klammern genutzt. Die gilt allerdings nicht für Attribute, die
innerhalb eines Templates gesetzt werden sollen. In dem Fall muss stattdessen auf die
\inlinecode{v-bind}-Direktive zurückgegriffen werden, die in Code-Beispiel
\ref{code:vueAttributeBinding} noch einmal dargestellt wird. Als weitere Direktiven existieren
\inlinecode{v-if} mittels derer ein Element in Abhängigkeit eines booleschen Werts angezeigt werden
kann sowie \inlinecode{v-on} für Funktionen, die durch ein \gls{event} ausgelöst werden sollen.
\cite[]{vueTemplateSyntax}

\begin{lstlisting}[float, caption=Beispiel Vue-Attribut-Binding, label=code:vueAttributeBinding]
<div v-bind:id="dynamicId"></div>
\end{lstlisting}

Zusätzlich zu \inlinecode{v-if} existieren als weitere Direktiven \inlinecode{v-else} und
\inlinecode{v-else-if}, die es ermöglichen komplexe bedingte Templates zu gestalten.
\cite[]{vueConditionalRendering}

Auch sich wiederholende Strukturen können in Vue mittels der Direktive \inlinecode{v-for} in einem
Template untergebracht werden. Ebenso ist es möglich einen Zahlwert an diese Direktive zu übergeben
und das Element entsprechend oft zu wiederholen. \cite[]{vueListRendering}

Vue implementiert ein virtuelles \ac{dom}, welches in seiner Funktionsweise nahezu identisch mit dem
von React ist. So werden die beschriebenen Templates zu Render-Funktionen kompiliert, die wiederum
das virtuelle \ac{dom} erzeugen. Diese Render-Funktionen entsprechen den im vorherigen Absatz
beschriebenen Render-Funktionen, die alternativ zu Templates geschrieben werden können. Dies,
zusammen mit dem Reactivity System, ermöglicht es Vue die Änderungen am realen \ac{dom} so gering
wie möglich zu halten, wenn sich der Zustand der Anwendung verändert. \cite[]{vueTemplateSyntax}

Komponenten in Vue können sowohl lokal, wie auch global registriert werden. Für die globale
Registrierung existiert die Funktion \inlinecode{Vue.component}, die als ersten Parameter den Namen
der Komponente erhält und als zweiten Parameter, ebenso wie eine Vue-Instanz, ein Options-Objekt.
Dieses kann nahezu alle Optionen enthalten, die auch das Objekt der Vue-Instanz besitzen kann.
Handelt es sich um eine Komponente, die etwas darstellen soll, so ist das
\inlinecode{template}-Element erforderlich, in dem das Template wie in Code-Beispiel
\ref{code:vueComponentGlobal} im String-Format enthalten ist. Unter dem Namen der Komponente ist
diese dann in jedes Template des Projektes wie ein gewöhnliches \ac{html}-Element integrierbar.
\cite[]{vueComponentsBasics}

\begin{lstlisting}[float, caption=Beispiel globale Vue-Komponente, label=code:vueComponentGlobal]
Vue.component('button-counter', {
	data: function () {
		return {
			count: 0
		}
	},
	template: `
		<button v-on:click="count++">
			You clicked me {{ count }} times.
		</button>
	`
})
\end{lstlisting}

Eine globale Registrierung kann bei Verwendung eines \gls{build}-Systems dazu führen, dass nicht
genutzte Komponenten trotzdem mit gepackt und ausgeliefert werden, womit die Downloadgröße der
Anwendung steigt. Aus diesem Grund kann es sinnvoll sein Komponenten lokal an den Stellen zu
registrieren, an denen sie benötigt werden. Dies geschieht, indem eine Komponente, genauer genommen
das Options-Objekt dieser Komponente, als JavaScript-Objekt gespeichert wird und dann in der
Komponente oder Vue-Instanz, die die Komponente nutzt, über das Element \inlinecode{components} des
Options-Objekts registriert wird. Code-Beispiel \ref{code:vueComponentLocal} zeigt das Äquivalent
mit lokaler Registrierung zu Code-Beispiel \ref{code:vueComponentGlobal}.
\cite[]{vueComponentRegistration}

\begin{lstlisting}[float, caption=Beispiel lokale Vue-Komponente, label=code:vueComponentLocal]
ComponentA = {
	data: function () {
		return {
			count: 0
		}
	},
	template: `
		<button v-on:click="count++">
			You clicked me {{ count }} times.
		</button>
	`
}

new Vue({
	el: '#app',
	components: {
		'component-a': ComponentA
	},
	template: `
		<component-a/>
	`
})
\end{lstlisting}

Eine weitere Möglichkeit, lokal registrierte Komponenten zu entwickeln, bietet Vue mit den
Eine-Datei-Komponenten\footnote{Im englischen Original \glqq Single File Components\grqq{}}. Diese
zeichnen sich dadurch aus, dass das Template, das Verhalten und das Styling in einer Datei definiert
und sowohl Styling als auch Template nicht im String-Format geschrieben werden. Erkennbar sind
solche Komponenten an der Dateiendung \inlinecode{.vue}. Möglich ist dies jedoch nur unter
Verwendung von \gls{build}-Werkzeugen wie zum Beispiel webpack. Ein Beispiel einer solchen
Komponente zeigt Code-Beispiel \ref{code:vueSingleFileComponent}. Eine Besonderheit ist hierbei das
Vue-spezifische Attribut \inlinecode{scoped} des \inlinecode{<style>}-Tags. Dieses besagt, dass der
definierte Style nur für diese Komponente gilt, also in der Komponente gekapselt ist. Sollte der
Wunsch bestehen, Template, Styling und Skript voneinander zu trennen, so ist dies auch möglich, indem
das Skript in eine separate JavaScript-Datei ausgelagert wird und das Styling in eine
\ac{css}-Datei, welche dann in die Komponente hinein geladen werden.
\cite[]{vueSingleFileComponents}

\begin{lstlisting}[float, caption=Beispiel Eine-Datei-Komponente in Vue, label=code:vueSingleFileComponent]
<template>
	<p>{{ greeting }} World!</p>
</template>

<script>
	module.exports = {
		data: function() {
			return {
				greeting: "Hello"
			};
		}
	};
</script>

<style scoped>
	p {
		font-size: 2em;
		text-align: center;
	}
</style>
\end{lstlisting}

Generell zu beachten ist bei Vue-Komponenten, dass das \inlinecode{data}-Objekt nicht wie bei der
Vue-Instanz direkt definiert werden darf, sondern über eine Funktion zurückgegeben werden muss.
Anderenfalls würden sämtliche Instanzen einer Komponente auf denselben Datenbestand zugreifen, wären
somit jederzeit im selben Zustand und würden dieselben Werte anzeigen. \cite[]{vueComponentsBasics}
