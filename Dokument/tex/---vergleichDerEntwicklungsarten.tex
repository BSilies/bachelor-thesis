\subsection{Vergleich der Entwicklungsarten}
Begonnen werden soll der Vergleich mit den Bewertungen hinsichtlich Wartbarkeit, Entwicklungskomfort
und Erlernbarkeit. Eine objektive Bewertung ist für diese Metriken nur bedingt möglich, da
insbesondere Entwicklungskomfort und Erlernbarkeit stark von den Vorerfahrungen und persönlichen
Präferenzen eines Entwicklers abhängen. Die Punkte, die zu diesen Bewertungen führen, werden jedoch
ausführlich erläutert und mögliche Einschränkungen und subjektive Ansichten als solche
gekennzeichnet.

Wird die Wartbarkeit des Quellcodes betrachtet, so ist diese aus Sicht des Autors durch Stencil am
besten gegeben. Erreicht wird dies insbesondere durch den Einsatz von \glspl{decorator}en, die den
Quellcode zum einen übersichtlicher machen, zum anderen aber auch eine kennzeichnende Wirkung haben,
für die in den anderen Implementierungen auf Kommentare zurückgegriffen werden müsste. Auch das
bereits mehrfach genannte Asset-Management in Stencil trägt durch Übersichtlichkeit zur Wartbarkeit
bei.

Die Wartbarkeit der Implementierungen in hybrids und LitElement liegt in etwa auf einer Stufe und
variiert zwischen den implementierten Komponenten. Bei Komponenten ohne nennenswerte eigene Logik,
wie \inlinecode{lsg-basic-headline} eine ist, kann hybrids besonders durch die kompakte
Implementierung punkten. Diese wird jedoch bei Komponenten mit einem hohen Logik-Anteil zum
Nachteil, wie die Komponente \inlinecode{lsg-accordion} zeigt (siehe Anhang \ref{chap:code}). Bei
dieser macht sich insbesondere das Fehlen eines Konstruktors bemerkbar, da Aufgaben wie die
Registrierung eines \gls{eventListener}s oder Mutations-Observers in diesem einen geeigneten Platz
finden. Der Verzicht auf die \gls{thisSyntax} wirkt sich ebenfalls negativ auf die Wartbarkeit aus,
da \inlinecode{this} in den meisten Fällen nur durch eine andere Variable ersetzt wird, dadurch aber
das Syntax-Highlighting diese Konstrukte nicht wie gewohnt markiert. Zusätzlich benötigt jede
Funktion in hybrids, die auf Properties der Komponente zugreifen muss, einen Parameter mehr als die
gleiche Funktion in einer anderen Entwicklungsart.

In der nativen Entwicklung wird die Wartbarkeit gegenüber den anderen Entwicklungsarten vor allem
durch die Masse des zu schreibenden Codes negativ beeinflusst. Wie das \ac{dom} in Abhängigkeit von
Änderungen der Properties oder Attribute einer Komponente aktualisiert werden soll, liegt
ausschließlich in der Hand des Entwicklers; diese Aufgabe wird in allen anderen Implementierungen
durch die Frameworks übernommen. In diesem Zusammenhang sei auch das Schreiben der Zugriffsmethoden
für die Properties genannt, da über diese die Aktualisierung des \ac{dom} angestoßen werden muss.
Die Erzeugung des Shadow \ac{dom} muss in der nativen Entwicklung ebenso übernommen werden, wie auch
die Aufgabe zu verhindern, dass Überlagerung von Properties durch vorzeitigen Zugriff stattfindet.
Auch diese Aufgaben werden durch die Frameworks übernommen. Die Aufgabe die grundlegende Wartbarkeit
der Komponente aufrecht zu erhalten, liegt bei diesem Ansatz somit noch mehr bei dem Entwickler als
bei allen anderen Ansätzen; es liegt in seiner Verantwortung den Quellcode übersichtlich zu
strukturieren.

Die getroffene Bewertung soll jedoch nicht heißen, dass Stencil-Code grundsätzlich besser wartbar
ist als nativ entwickelte Komponenten. Durch unsaubere Strukturierung kann jeder geschriebene
Quellcode schlecht wartbar werden, wodurch die Verantwortung für die Wartbarkeit des Codes
grundsätzlich immer beim Entwickler liegt. Frameworks und Bibliotheken können diesen Prozess
lediglich positiv beeinflussen.

Auch im Entwicklungskomfort kann Stencil am meisten überzeugen, was auf mehrere Gründe
zurückzuführen ist. Schon bei der Initialisierung des Projektes unterstützt Stencil durch einen
Initializer für den Paketmanager npm, der ein lauffähiges Projekt inklusive aller notwendigen
Konfigurationen erzeugt. In diesem Projekt mussten die Konfigurationen nicht nennenswert angepasst
werden, sodass nahezu die gesamte Zeit in die Komponentenentwicklung investiert werden konnte. Neben
dem Compiler liefert Stencil auch einen schlanken Entwicklungsserver, der jedoch im Praxiseinsatz
nicht so sehr überzeugen konnte wie der für die anderen Entwicklungsarten verwendete. Die Nutzung
der \glspl{decorator}en in Stencil empfindet der Autor als positiv für den Entwicklungskomfort
ebenso wie das Asset-Management, da so die Menge des zu schreibenden Quellcodes stark reduziert
wird. Auch wenn im Vergleich zu hybrids gemessen an Code-Zeilen mehr Quellcode geschrieben werden
musste, wirkt die Arbeit dennoch übersichtlicher. Dies ist in erster Linie auf viele Leerzeilen und
Zeilenumbrüche zurückzuführen, da die Größe der Komponenten gemessen am Speicherbedarf bei Stencil
in vier von sechs Fällen geringer als bei hybrids ist (siehe Anhang \ref{chap:groesseKomponente}).
Die bereits in der Analyse angesprochene automatische Erzeugung einer Dokumentation für eine
Komponente ist ebenfalls positiv hervorzuheben, da diese Aufgabe sonst händisch durch den Entwickler
ausgeführt werden müsste. So müssen lediglich Details ergänzt werden, die nicht automatisch
generierbar sind.

Der Komfort in LitElement ist ebenfalls als positiv zu bewerten, da auch hier eine übersichtliche
Strukturierung des Quellcodes erleichtert wird. Inwieweit der Einsatz der gebotenen
\glspl{decorator}en den Entwicklungskomfort weiter erhöhen kann, wurde nicht untersucht. Erwartet
wird, dass dieser in geringem Maße durch vereinfachte Definition des Custom Elements und der
Properties zunimmt. Positiv zum Entwicklungskomfort trägt, ebenso wie bei Stencil, eine gute
Dokumentation des Frameworks bei. Diese ist in hybrids an einigen Stellen sehr knapp, sodass unter
Umständen gewisse Fragen offen bleiben. Dies hat wahrscheinlich dazu beigetragen, dass die
Verwendung der \inlinecode{children}-Factory in der Accordion-Komponente zurückgestellt werden
musste.

Der klassenlose Ansatz von hybrids stellt insbesondere Entwickler, die bisher mit objektorientierten
oder klassenbasierten Sprachen beziehungsweise Frameworks gearbeitet haben, vor eine kleine
Herausforderung. So ist zum Beispiel durch den Wegfall der \gls{thisSyntax} und eines Konstruktors
ein Umdenken erforderlich, was zu Lasten der Produktivität geht. Für Entwickler, die bereits
Erfahrung mit Vue haben, könnte dieses Framework jedoch leichter zu nutzen sein, da auch die
Vue-Instanz mittels eines Objektes definiert wird.

In der nativen Entwicklung muss komplett auf den Komfort von Frameworks verzichtet werden, was in
erster Linie in deutlich mehr zu schreibendem Code resultiert, wie auch Anhang
\ref{chap:groesseKomponente} zeigt. Der gebotene Komfort beschränkt sich in erster Linie auf eine
gute Dokumentation der Standards durch die \cite{mdnWebcomponents}.

Der nativen Entwicklung, LitElement und hybrids ist eine Einschränkung des Komforts gemein. Das
Template wird in allen drei Varianten mittels Template Literalen geschrieben, die von Editoren unter
Umständen als Strings behandelt werden und somit kein Syntax-Highlighting hierfür bieten
beziehungsweise dies nur durch Erweiterungen für den Editor möglich ist.

Betrachtet man die Erlernbarkeit der Frameworks, so ist in der nativen Entwicklung der Lernaufwand
in erster Linie auf die Konzepte der Standards zu Custom Elements, \ac{html} Templates und dem
Shadow \ac{dom} beschränkt. Dies gilt allerdings nur, sofern eine gute Kenntnis der Grundlagen für
die Webentwicklung vorliegt. Hierzu zählen die Sprachen \ac{html}, \ac{css} und JavaScript, aber
auch das \ac{dom}-\ac{api}. Mit Ausnahme des letzten Punktes sind dies auch unabdingbare Grundlagen
für die Entwicklung mittels der Frameworks. Die Entwicklung mittels LitElement kann ohne Kenntnis
der Webstandards erfolgen, da die benötigten Punkte aus diesen auch in der Dokumentation der
Bibliothek aufgeführt werden. Da der Umfang der Bibliothek verhältnismäßig gering ist, wird auch für
die Einarbeitung nur wenig Zeit benötigt. Unterstützt wird die Einarbeitung durch eine ausführliche
Dokumentation der Bibliothek.

Die Einarbeitung in hybrids ist durch das klassenlose Konzept für Entwickler, die nicht mit
ähnlichen Konzepten vertraut sind, aufwändiger als dies zum Beispiel bei LitElement der Fall ist. Im
Umfang der Bibliotheken ähneln sich LitElement und hybrids. Jedoch wirkt die Dokumentation von
hybrids an einigen Stellen unausgereift und unvollständig. Dies erschwert die Einarbeitung
zusätzlich. Der Einstieg in die Entwicklung mit Stencil ist insbesondere für Entwickler, die bereits
Erfahrung mit React haben, einfach. Wie React setzt auch Stencil auf State, Props und die
Template-Sprache \ac{jsx}. Auch die \glspl{lifecycleCallback} sind ähnlich benannt und ein
offensichtlicher Unterschied kann lediglich in den \glspl{decorator}en und der Art State und Props
zu notieren, ausgemacht werden. Im Detail sind aber auch in Stencil die Ähnlichkeiten zu den
Webcomponents-Standards sichtbar, sodass auch Entwickler, die bereits Erfahrungen mit Webcomponents
haben, einen relativen einfachen Einstieg haben. \ac{jsx} verwendet für die Definition der Templates
ähnliche Konstrukte wie auch LitElement und ist in seiner Syntax an \ac{html} angelehnt. Somit ist
der Ein- beziehungsweise Umstieg auf diese Sprache verhältnismäßig simpel. Auch die Nutzung von
TypeScript ist für Entwickler mit JavaScript-Erfahrung, aber insbesondere Entwickler anderer
typisierter Sprachen kein großes Hindernis. Die entwickelten Algorithmen unterscheiden sich in
TypeScript praktisch nicht von JavaScript, lediglich die Deklaration von Variablen wird durch einen
Datentyp ergänzt.

Zuletzt soll auch auf den \gls{bundeler} webpack eingegangen werden, der für die native Entwicklung
und die Entwicklung mittels LitElement und hybrids eingesetzt wurde. Dieser bietet auf seiner Website
\cite[]{webpack} eine ausführliche Anleitung, wie ein Projekt aufgesetzt werden kann. Ein Projekt
ohne Vorlagen und Beispiele aufzusetzen ist insbesondere für Anfänger schwierig. Werden jedoch die
Guides \cite[]{webpackGuides} genutzt, um sich das grundsätzliche Verständnis anzueignen und einige
beispielhafte Konfigurationen kennenzulernen, können diese dann mit geringem Aufwand auf das eigene
Projekt angepasst werden. Im Vergleich zu den Frameworks ist der Lernaufwand hierfür jedoch
wahrscheinlich am höchsten, sofern annähernd verstanden werden soll, was die aktuelle Konfiguration
bewirkt und wie bestimmte gewünschte Effekte erreicht werden können.

Im nun folgenden Teil soll die Performance der Implementierungen genauer untersucht werden. Hierzu
wurden zwei Testreihen durchgeführt, die unterschiedliche Aspekte der Performance untersucht haben.

Zuerst wurden die Implementierungen hinsichtlich ihrer Ladeperformance untersucht. Genutzt wurde
hierfür das \ac{api} von Googles \glqq PageSpeed Insights\grqq{}. \cite[]{googlePageSpeedInsights}
Für die Tests wurden sechs \ac{html}-Seiten pro Komponente erstellt, um das Ladeverhalten in
verschiedenen Szenarien untersuchen zu können. Die Seiten sind wie folgt zu beschreiben:

\begin{enumerate}
	\item \textbf{Minimal:} Die Seite enthält ein Accordion im Modus \glqq Multiple\grqq{} mit einem
	      Accordion-Element.
	\item \textbf{Mixed Small:} Die Seite enthält je ein Accordion pro Modus mit je fünf
	      Accordion-Elementen.
	\item \textbf{Mixed Medium:} Die Seite enthält je zwei Accordions pro Modus mit je 20
	      Accordion-Elementen.
	\item \textbf{Multiple:} Die Seite enthält zehn Accordions im Modus \glqq Multiple\grqq{} mit je
	      100 Accordion-Elementen.
	\item \textbf{Single:} Die Seite enthält zehn Accordions im Modus \glqq Single\grqq{} mit je 100
	      Accordion-Elementen.
	\item \textbf{Strict:} Die Seite enthält zehn Accordions im Modus \glqq Strict\grqq{} mit je 100
	      Accordion-Elementen.
\end{enumerate}

Zur Ausführung des Tests wurde ein Skript geschrieben, welches für jede der insgesamt 24 Seiten 100
Analyse-Ergebnisse mit der Einstellung \glqq Desktop\grqq{} anfordert und aus diesen das
arithmetische Mittel, sowie die Standardabweichung berechnet. Dargestellt werden die Ergebnisse in
Tabellenform auf einer \ac{html}-Seite. Ausgeführt wurde der Test im Browser Firefox mit der
Versionsnummer 77.0.1. Die Ergebnisse der Tests sind in Anhang \ref{chap:loadPerformance} zu sehen,
von denen jedoch nicht alle Metriken diskutiert werden sollen. Als Grundlage für den Vergleich dient
die Messung nach Optimierung des Quellcodes.

Am einfachsten zu interpretieren ist die Metrik \glqq Time to Interactive\grqq{}, die angibt, nach
welcher Zeit die Seite vollständig interaktiv ist. Bei allen Seiten schneidet Stencil vor hybrids
und der nativen Implementierung am besten ab. Die längste Zeit benötigt LitElement. Wird die
Seitenvariante Minimal betrachtet, ist festzuhalten, dass Stencil diese bereits nach 302ms
vollständig geladen hat, wohingegen LitElement mit 537~ms über eine halbe Sekunde benötigt. Der
Unterschied zwischen der nativen Implementierung mit 485~ms und hybrids mit 480~ms ist minimal.
Betrachtet man die Seiten-Varianten Multiple, Single und Strict als das andere Extrem, so liegen die
Werte dort für die native Entwicklung bei gemittelt 5019ms, für LitElement bei 6251ms, für hybrids
deutlich niedriger bei 1791~ms und für Stencil sogar bei 1692ms. Der Anstieg über die gemessenen
Zeiten der anderen beiden Seiten verläuft für alle Implementierungen in etwa linear in Abhängigkeit
der zu rendernden Elemente. Bemerkenswert ist, dass die Zeiten von hybrids trotz ähnlichem Wert im
Minimalbeispiel deutlich langsamer zunehmen als die der nativen Implementierung und sich eher den
Werten von Stencil annähern.

Die zweite offensichtliche Metrik ist \glqq Total Byte Weight\grqq{} in Kombination mit \glqq Unused
JavaScript\grqq{}. Diese geben an, wie viele Daten zur Darstellung einer Seite vom Server
heruntergeladen werden und wie viele JavaScript-Code davon ungenutzt bleiben. Auch hier liegt
Stencil im Vergleich vorne, in dieser Metrik jedoch gefolgt von der nativen Entwicklung, hybrids und
LitElement in dieser Reihenfolge. Für das Minimalbeispiel lädt Stencil etwa 12~kB Daten, die native
Entwicklung circa 22~kB, hybrids etwa 30~kB und LitElement benötigt mit 47~kB über doppelt so viel wie
die native Entwicklung. Diese Metrik ist insbesondere aus Sicht der mobilen Nutzung einer Seite
interessant, da die verfügbare Bandbreite im deutschen Mobilfunknetz häufig geringer ist als bei
einem Kabelanschluss. Somit wirken sich große Datenmengen durch lange Download-Zeiten auch negativ
auf die Ladezeit einer Seite aus. Die ungenutzte Menge JavaScript-Code kann hierbei als
Optimierungsbedarf angesehen werden. Die native Variante des Accordions lädt hierbei etwa 4~kB
ungenutzten JavaScript-Code herunter, was etwa 20~\% der gesamten Datenmenge im Minimalbeispiel
entspricht. Bei LitElement sind dies mit gut 13~kB sogar etwa 30~\%. Mit knapp 27~\% liegt hybrids
zwischen den vorher genannten Implementierungen. Einzig Stencil lädt laut Messungen keinen
ungenutzten JavaScript-Code herunter. Eine genauere Bewertung hierzu erfolgt im nächsten Abschnitt
\ref{ssec:bewertung}.

Der Punkt \glqq Speed Index\grqq{} soll in diesem Zusammenhang diskutiert werden, da er bei den
großen Dateien Multiple, Single und Strict ein unerwartetes Ergebnis liefert. Während Stencil bei
den anderen drei Dateien jeweils den besten Wert aufweist, reiht sich das Framework bei diesen
Dateien nach allen anderen Implementierungen ein. Der \glqq Speed Index\grqq{} gibt an, wie schnell
Inhalte nach dem Laden der Seite angezeigt werden. Stencil scheint, wie durch Beobachtungen
festgestellt werden konnte, bei diesen Dateien die Darstellung der Inhalte zu verzögern bis alle
Elemente auf der Seite vollständig geladen sind. Hingegen präsentiert hybrids auch Inhalte, bevor
die Komponenten geladen wurden, erkennbar daran, dass ungestylter Text angezeigt wird.

Alle weiteren Punkte bestätigen das Ergebnis, dass Stencil vor hybrids mit Ausnahme des eben
genannten Punktes die beste Ladeperformance aufweist. Die native Implementierung folgt teils mit
großem Abstand. Am schlechtesten schneidet LitElement in diesem Test ab.

In einem weiteren Test wurde die Performance zur Laufzeit bei zwei Aufgaben in unterschiedlichen
Ausprägungen untersucht. Untersucht wurde zum einen, wie schnell ein Accordion-Element unter
verschiedenen Bedingungen ausgeklappt wird, zum anderen aber auch, wie schnell ein Accordion die
Änderung des Modus unter verschiedenen Bedingungen verarbeitet. Hierzu wurde eine neue Seite je
Implementierung gestaltet, die je ein Accordion pro Modus mit je 100 Accordion-Elementen enthält. Um
die Tests vorzubereiten, wurde das Tool Puppetry verwendet, welches Tests für das Test-Framework
Jest mit Nutzung des Puppeteer-\ac{api}s generiert. \cite[]{puppetry} Die erstellten Tests wurden
exportiert und für alle Implementierungen angepasst. Dies bedeutete zum einen die Tests in einer
Schleife ausführen zu lassen, da auch hier 100 Ergebnisse pro Test gesammelt wurden. Zum anderen
mussten zur Bestimmung der Zeit auf die Performance-Metriken der Seite zugegriffen werden. Hierfür
mussten geringfügige Änderungen am Quellcode vorgenommen werden, um bei bestimmten Aktionen einen
Performance-Zeitstempel durch \inlinecode{console.timestamp()} zu setzen. Anhand dieser kann die
Zeit für eine Aktion berechnet werden. Die genannten Aktionen sind wie folgt zu beschreiben:

\begin{samepage}
	\begin{itemize}
		\item Das Accordion-Element empfängt ein Klick-\gls{event}.
		\item Das Accordion hat nach einem Klick-\gls{event} die entsprechende Funktion zum
		      Aktualisieren der Zustände aller Accordion-Elemente zu Ende ausgeführt.
		\item Das Accordion detektiert eine Änderung des Modus.
		\item Das Accordion hat nach Änderung des Modus die entsprechende Funktion zum Aktualisieren
		      der Zustände aller Accordion-Elemente zu Ende ausgeführt.
	\end{itemize}
\end{samepage}

Eine genaue Beschreibung der Testfälle, sowie die Ergebnisse sind in Anhang
\ref{chap:runtimePerformance} zu finden. Ausgeführt wurden die Tests mittels npm in der
Browser-Variante \glqq Headless Chromium\grqq{}\footnote{Chromium ist das Open-Source
Browser-Projekt auf dem die Browser Google Chrome, sowie der neue Microsoft Edge basieren. Headless
gibt an, dass der Browser kein nennenswertes \ac{ui} besitzt. Der Browser eignet sich besonders für
computergesteuerte Tests oder andere automatisierte Aufgaben.}. Maßgeblich soll auch hier die
Messung nach Optimierung des Quellcodes sein. In einigen Punkten der Auswertung wurden Annahmen zu
der Performance getroffen, die von den tatsächlich Messwerten abweichen. Eine Begründung hierzu
folgt im nächsten Abschnitt \ref{ssec:bewertung}.

In der ersten Test-Kategorie, dem Ausklappen einzelner Accordion Elemente unter verschiedenen
Bedingungen, wird ersichtlich, dass LitElement mit Ausnahme zweier Testfälle die performanteste
Lösung bietet. Diese ist in allen Fällen über doppelt so schnell wie die der Implementierung ohne
Framework. Hybrids bietet in den meisten Fällen vor Stencil die zweitbeste Lösung hinsichtlich
Performance in dieser Kategorie. Die native Implementierung benötigt für das Ausklappen des
Accordion-Elements in allen Varianten am meisten Zeit. So wird die einfachste Aufgabe, das
Ausklappen des ersten Accordion-Elements eines Accordions im Modus \glqq Multiple\grqq{} von
LitElement in 0,20~ms vor hybrids mit 0,22~ms und Stencil mit 0,28~ms bewältigt. Die Variante der
nativen Implementierung benötigt hierfür 0,42ms. Festgestellt werden kann auch, dass in den Modi
\glqq Single\grqq{} und \glqq Strict\grqq{} das Öffnen eines anderen Accordion-Elements, wenn das
erste aktuell geöffnet ist, durch hybrids im Mittel performanter umgesetzt wird als von Stencil,
Stencil jedoch im umgekehrten Szenario besser abschneidet. Als längste Ausführungszeiten haben die
Implementierungen jeweils in unterschiedlichen Tests folgende Werte: Die native Implementierung
0,50ms, LitElement 0,24ms, hybrids 0,39~ms und Stencil 0,44ms.

In der zweiten Testkategorie, dem Ändern des Modus eines Accordions unter verschiedenen Bedingungen,
zeigt sich, dass auch hier LitElement die beste Performance aufweist. Hybrids agiert in vier von
sechs Testfällen schneller als Stencil, dessen Implementierung nur in den einfachen Testfällen
schneller ist. Die Implementierung ohne Framework liegt mit großem Abstand hinter allen anderen. Die
zwei einfachsten Aufgaben -- der Wechsel in den Modus \glqq Strict\grqq{} aus den anderen beiden
Modi, ohne dass ein Accordion-Element geöffnet ist -- werden von LitElement in je 0,11~ms gelöst, da
diese auch die selben Anforderungen an die Komponente stellen. Auch der Wechsel des Modus von \glqq
Multiple\grqq{} auf \glqq Single\grqq{} oder \glqq Strict\grqq{} unter den gleichen Vorbedingungen
führt zu annähernd gleichen Messwerten. Mit 0,27~ms für den Wechsel des Modus von \glqq
Multiple\grqq{} auf \glqq Strict\grqq{} bei elf geöffneten Elementen ist die Implementierung in
LitElement etwa doppelt so schnell wie die Stencil-Variante. Hybrids ordnet sich mit 0,41~ms auf
Platz zwei ein, während die native Variante mit 1,46~ms fast drei mal so lange braucht wie die
Implementierung in Stencil.

Interessant ist auch ein Vergleich der Ergebnisse der optimierten mit denen der ersten
implementierten Version. Den größten Vorteil in der Laufzeitperformance brachte die Optimierung der
nativen Entwicklung. In den Tests, in denen die Zeiten zum Ausklappen des Accordions untersucht
werden, konnte festgestellt werden, dass die optimierte Verarbeitung in der nativen Implementierung
teils um über den Faktor 20 schneller ist als in der ursprünglichen Version. So wurde dort als
höchster Wert 11,03~ms für das Öffnen des ersten Accordion-Elements inklusive aller nötigen Prüfungen
eines Single-Accordions gemessen. Für die Multiple-Accordions hingegen hatte die Optimierung keine
signifikante Auswirkung auf die Performance. Aber auch die übrigen Implementierungen haben von
dieser Optimierung profitiert, sodass LitElement von der im Mittel drittschnellsten Lösung zur
schnellsten wurde. Auch hybrids konnte sich gegenüber Stencil verbessern, bei dessen Implementierung
die Optimierung nur zu maximal einer Halbierung der Zeit führte. In der zweiten Testgruppe
profitierte LitElement am meisten, während die Optimierung für hybrids nur einen geringen
Unterscheid machte. Negativ wirkte sich die Optimierung lediglich auf die Größe der Komponente,
gemessen am \glqq Total Byte Weight\grqq{} der Ladeperformance, aus. Andere Auswirkungen auf die
Ladeperformance sind nicht zweifelsfrei zu erkennen.

Insgesamt ist festzuhalten, dass Stencil im Rahmen dieser Arbeit die beste Möglichkeit geboten hat,
die gestellte Aufgabe zu erfüllen. Dies ist zum einen im Entwicklungskomfort mit dem geringen
Aufwand zur Initialisierung des Projektes und weiterer Unterstützung im Entwicklungsprozess zu
begründen, zum anderen aber auch durch das Ergebnis des Performance-Tests des ausgelieferten
Produktes.
