\subsection{Bewertung der Ergebnisse}
\label{ssec:bewertung}
Die Ergebnisse des im vorigen Abschnitt durchgeführten Vergleichs kommen nicht ohne Bewertung aus.
Betrachtet man den ersten Teil des Vergleichs, in dem in erster Linie die den Entwicklungsprozess
beeinflussenden Faktoren untersucht wurden, so ist festzustellen, dass eine objektive Bewertung nur
bedingt möglich ist. Die Bewertung erfolgte ausschließlich auf Basis der Implementierungen, die im
Rahmen dieser Arbeit angefertigt wurden. Dass dies repräsentativ für alle Arten von Projekten gilt,
kann nicht angenommen werden. Auch die Vorerfahrungen und persönlichen Vorlieben eines Entwicklers
haben großen Einfluss vor allem auf die Punkte Erlernbarkeit und Entwicklungskomfort.

Werden die Punkte Wartbarkeit, Erlernbarkeit und Entwicklungskomfort als Einheit betrachtet, so kann
nach den im vorigen Abschnitt getroffenen Bewertungen naiver Weise zu folgendem Schluss gekommen
werden: Stencil ist das beste der drei genannten Frameworks, hybrids schafft unnötige Komplexität,
LitElement reduziert lediglich Schreibarbeit und die native Entwicklung ist aufgrund der vielen
Nachteile gar nicht zu empfehlen. Diese Schlussfolgerung kann und darf so jedoch nicht stattfinden.

Die native Entwicklung ist wahrscheinlich für große Entwicklungsprojekte nicht die richtige Wahl, da
diese mit größerem Implementierungsaufwand verbunden ist, als dies bei der Nutzung eines Frameworks
der Fall ist. Dennoch empfiehlt sich die Beschäftigung mit dieser Art zu implementieren insbesondere
beim Einstieg in das Thema Webcomponents. Grund hierfür ist, dass so ein tieferliegendes Verständnis
für die Standards und Konzepte der Webcomponents hergestellt wird, welches bei der Implementierung
mittels Frameworks von Nutzen sein kann.

Dass LitElement nicht nur Schreibarbeit reduziert, zeigt sich vor allem an den Ergebnissen des
Laufzeitperformance-Tests. Gegenüber den anderen Implementierungen kann LitElement hier
offensichtlich mit einem besonders effizienten \ac{dom}-Updateverhalten punkten. Diese Bibliothek
eignet sich somit für Entwicklungen, die weitestgehend ohne Framework auskommen sollen, jedoch nicht
sämtliche Performance-Optimierungen durch den Entwickler geschehen müssen.

Die in diesem Projekt festgestellte Komplexität von hybrids muss sich nicht zwangsläufig einstellen.
So eignet sich hybrids zum Beispiel für die Entwicklung einfacher Komponenten ohne nennenswerte
Logik, da der notwendige Quellcode kompakt geschrieben werden kann. Auch dort, wo viele Properties
eine ähnliche Struktur aufweisen und sie bei Änderungen das gleiche Verhalten hervorrufen, kann
hybrids durch seine Factories punkten.

In Stencil überzeugt in der Tat das Gesamtpaket, insbesondere die Unterstützung des
Entwicklungsprozesses durch das Framework. Hierdurch qualifiziert sich Stencil auch für größere
Projekte und stellt eine legitime Alternative zu anderen großen JavaScript-Frameworks wie React dar.
In der Laufzeitperformance schneidet Stencil von allen Frameworks jedoch am schlechtesten ab.

Der angestellte Vergleich zur Ladeperformance ist nur bedingt aussagekräftig. Die Ergebnisse der
nativen Implementierung sowie derer mit LitElement und hybrids können gut verglichen werden, da
derselbe \gls{bundeler} mit annähernd identischer Konfiguration verwendet wurde. Erzeugt wurde hier
jedoch nur eine Version, die tendenziell auch in älteren Browsern funktioniert, während Stencil
neben einer solchen Version auch eine bietet, die auf neue Browser ausgelegt ist. In den
Performance-Messungen kam letztgenannte Version zum Einsatz, die im Vergleich zu den anderen
Implementierungen dadurch schlanker ist, dass diese nicht in den ECMAScript 5-Standard übersetzt
wurde und keine Polyfills lädt. Für eine bessere Vergleichbarkeit der Implementierung in Stencil mit
den übrigen Implementierungen wäre somit der \gls{build} einer weiteren Version der anderen
Implementierungen notwendig.

Auch die Ergebnisse des Laufzeitperformance-Tests müssen insofern kritisch betrachtet werden, dass
die verwendeten Messwerte zum Teil eine Standardabweichung von über 100~\% haben. Insbesondere in den
extremen Fällen muss dies auf vereinzelte Extremwerte zurückgeführt werden, jedoch ist auch sonst
eine breite Streuung der Messwerte zu beobachten. Auf eine Bereinigung der Messwerte wurde bewusst
verzichtet, da auch unter realen Bedingungen extreme Abweichungen auftreten können. In der im
vorigen Abschnitt durchgeführten Auswertung dieser Daten wurden für die Werte, die durch hohe
Standardabweichungen für starke Extremwerte in der Messung sprechen, stattdessen Annahmen für eine
bessere Vergleichbarkeit getroffen. Diese basieren in erster Linie auf den Messungen in anderen
Tests, in denen durch ähnliches getestetes Verhalten ähnliche Messergebnisse erwartet werden. Zu
beachten ist auch, dass die gemessenen Zeiten der optimierten Variante zum Großteil unter einer
Millisekunde liegen und das Ergebnis jeder Messreihe unter zwei Millisekunden liegt. Aus diesem
Grund fallen bereits kleinste Abweichungen stark ins Gewicht.

Die Vergleichbarkeit der Ergebnisse ist auch hier in geringem Maße eingeschränkt, da die
beschriebenen Messpunkte nicht in jeder Implementierung an derselben Stelle gesetzt werden konnten.
Dies ist vor allem in der Teils verkürzten Schreibweise der Frameworks begründet, wodurch auf
Messpunkte, die dem eigentlich notwendigen Messpunkt am nächsten kommen, zurückgegriffen werden
musste. Der Fall war dies besonders bei dem Messpunkt, der das Setzen des Modus markiert, da alle
vier Implementierungen abweichende Punkte zueinander haben, an denen die Änderung festgestellt
werden kann.

Der Vergleich der Performance-Messungen von optimierter und ursprünglicher Variante zeigt einmal
mehr, dass Frameworks Standardaufgaben nicht nur übernehmen, sondern diese häufig auch stark
optimiert ausführen. Erkennbar ist dies daran, dass der Unterschied der Änderung durch die
Optimierung insbesondere in Stencil und hybrids deutlich geringer ausfällt als in der nativen
Implementierung oder LitElement. Gleichzeitig verdeutlicht dies aber auch noch einmal die
Verantwortung des Entwicklers, der ohne Framework-Unterstützung selbst für eine gute Performance
seiner Implementierung sorgen muss. Betrachtet man die Tatsache, dass die nativ implementierte
Variante in allen Laufzeitperformance-Tests am schlechtesten abschneidet, so muss auch an dieser
Stelle zugegeben werden, dass noch Optimierungspotential besteht.

Zuletzt sei aber auch dieser Punkt noch ein wenig relativiert, da die gemessenen Zeiten im Mittel
alle unter zwei Millisekunden, für das Öffnen eines Accordion-Elements sogar bei einer halben
Millisekunde und darunter liegen. Die Unterschiede der gemessenen Zeiten sind aus Sicht der \ac{ux}
irrelevant, da Reaktionszeiten unter 100 Millisekunden durch den Benutzer als sofortige Reaktion
betrachtet werden. Auch die Ladezeit, gemessen an der \glqq Time to Interactive\grqq{}, ist für alle
Implementierungen vertretbar. Die Seitenvarianten \glqq Multiple\grqq{}, \glqq Single\grqq{} und
\glqq Strict\grqq{} bilden mit 1010 \ac{dom}-Elementen Extremwerte ab, sodass diese nicht als Maß
gesehen werden können. Die Seite \glqq Mixed Medium\grqq{} kommt mit 126 \ac{dom}-Knoten am ehesten
einer kleinen Webpräsenz gleich. Diese wird in allen Varianten in unter 1,2 Sekunden geladen und ist
somit praxistauglich, auch wenn kürzere Ladezeiten insbesondere für die Nutzung auf mobilen
Endgeräten von Vorteil sind. \cite[]{usabilityblogPerformanceUX}
