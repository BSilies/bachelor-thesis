\subsection{Angular}
Angular ist ein Framework zur Entwicklung von \acp{spa} und wurde 2016 mit der Versionsnummer 2
veröffentlicht. Angular 1, auch bekannt als Angular JS wurde bereits 2010 veröffentlicht, wurde
jedoch mit Angular 2 vollständig überarbeitet und eine Rückwärtskompatibilität ist nicht gegeben.

Entwickelt wurde Angular in TypeScript, einer JavaScript-Erweiterung, die der Script-Sprache
Typisierung, Klassen und Interfaces, generische Parameter und einige weitere Funktionen hinzufügt.
Einige dieser Funktionen sind auch im aktuellen JavaScript-Standard vorhanden, waren dies zum
Zeitpunkt der Veröffentlichung im Jahr 2012 durch Microsoft jedoch noch nicht.
\cite[]{bochkorKrypczykEntwicklerDeTypeScript} Auch wenn die Entwicklung einer Angular-Anwendung mit
reinem JavaScript möglich ist, wird jedoch ausdrücklich empfohlen TypeScript zu verwenden.

Angular besitzt im Gegensatz zu React neben Komponenten noch Module, Services, Templates und Views.
Ein Service ist hierbei eine Funktionalität oder Gruppe von Funktionalitäten, die nicht einer
spezifischen Komponente zugeordnet werden können, sondern vielmehr allgemein benötigte
Funktionalität darstellen. Ein Beispiel kann die Verwendung eines \inlinecode{HttpClient}-Services
sein, wie er im \glqq Getting Started\grqq{}-Bereich von Angular beschrieben wird. Über diesen
Service werden sämtliche \ac{http}-Requests verarbeitet, sodass die Komponenten diese Funktionalität
nicht selbst implementieren müssen. Die Nutzung dieser Services wird in Angular durch Dependency
Injection erleichtert. So liefert Angular einen Injector mit, der einer Komponente die benötigten
Ressourcen und Services bereitstellt. Diese Ressourcen und Services können entweder Anwendungs-,
Modul- oder Komponentenweit zur Verfügung stehen. Von einer Ressource auf Anwendungsebene steht für
die gesamte Anwendung also nur eine Instanz zur Verfügung. Eine Ressource auf Komponentenebene
existiert jedoch mit einer Instanz pro Komponenteninstanz. Angular definiert eine Reihe von
\glspl{decorator}en\footnote{Elemente, die einer Klasse, Funktion oder Property zusätzliches
Verhalten hinzufügen können.}, darunter auch \textbf{@}\inlinecode{Injectable}, welcher verwendet
wird, um Ressourcen und Services für Dependency Injection bereitzustellen. Ein Beispiel zeigt das
Code-Beispiel \ref{code:serviceAngular}.\cite[]{angularServicesDi}

\begin{lstlisting}[float, caption=Service in Angular, label=code:serviceAngular]
@Injectable()
export class MyService {
	// Verhalten des Service
}
\end{lstlisting}

Module in Angular sind übergeordnete Strukturen, die Komponenten, Services, sowie weitere
Quellcode-Dateien bündeln. In der Regel werden Bestandteile einer Anwendung zu einem Modul
zusammengefasst, die eine Domäne bilden, einen spezifischen Workflow darstellen oder eine andere
enge Verwandtschaft aufweisen. Außerdem dienen Module als Kompilations-Kontext für Komponenten, die
in diesen beheimatet sind. Module können selbst Funktionalitäten weiterer Module importieren sowie
eigene Funktionalitäten für andere Module exportieren. Jede Angular-Anwendung benötigt mindestens
ein Modul, das sogenannte \glqq root module\grqq, welches für den Start der Anwendung herangezogen
wird. Ein Modul wird ebenfalls über einen \gls{decorator} gekennzeichnet. Hierzu wird die
entsprechende Klasse mit \textbf{@}\inlinecode{NgModule} versehen. Diesem \gls{decorator} können
eine Reihe Parameter mitgegeben werden, dessen Bedeutung in Code-Beispiel \ref{code:moduleAngular}
gezeigt wird. \cite[]{angularModules}

\begin{lstlisting}[float, caption=Modul in Angular, label=code:moduleAngular]
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
@NgModule({
	imports:      [ BrowserModule ], // Benötigte Module
	providers:    [ Logger ],        // Klassen, die Services definieren
	                                 // und Modulweit zugänglich sind
	declarations: [ AppComponent ],  // Alle Elemente, die zu der
	                                 // Komponente gehören
	exports:      [ AppComponent ],  // Elemente, die in anderen
	                                 // Komponenten genutzt werden können
	bootstrap:    [ AppComponent ]   // Nur im root Modul verwenden:
	                                 // Komponente, die Host aller
	                                 // weiteren Komponenten ist
})
export class AppModule { }
\end{lstlisting}

Eine Komponente ist in Angular anders als in Kapitel \ref{sec:komponentenInDerSoftwareentwicklung}
definiert. Eine Komponente nach Definition dieser Arbeit setzt sich in Angular aus einer
Angular-Komponente und einem Template zusammen und wird in Angular als View bezeichnet. Ist im
Folgenden von Komponenten die Rede, so sind Angular-Komponenten gemeint. Eine Komponente in Angular
definiert das Verhalten einer View, während das Aussehen durch ein Template definiert wird. Ein
Angular-Template ist in erster Linie eine \ac{html}-Datei, die durch zusätzliche Angular-Syntax
erweitert wird. Direktiven und Angulars Data-Binding sind Teil dieser Syntax und bestimmen
maßgeblich die Interaktion zwischen Komponenten und ihren Templates. Eine dritte Syntax-Erweiterung
ist die Verwendung eines Komponenten-Selektors als Tag-Namen in einer Template-Datei.

Das Data-Binding in Angular übernimmt die Aufgabe, Daten aus der Komponente in das \ac{dom} zu
schreiben, sowie durch \glspl{event} bestimmtes Verhalten auszulösen. Unterschieden werden vier
Arten des Data-Bindings, die in Code-Beispiel \ref{code:dataBindingAngular} dargestellt werden. Die
erste Art, Interpolation, gibt einen Wert der Komponente aus. Dies geschieht durch Einbettung des
auszugebenden Wertes in doppelte geschweifte Klammern, wie Zeile 1 des Beispiels zeigt. Das
Property-Binding übergibt einer View oder einem nativen \ac{html}-Element einen Wert der Komponente,
indem die zu befüllende Property in eckige Klammern geschrieben wird und den entsprechenden Wert wie
in Zeile 2 des Beispiels zugewiesen bekommt. Ein \gls{event}-Binding übernimmt die Rolle, die
\glspl{eventListener} in klassischem JavaScript haben und führt bei definierten \glspl{event} die
hinterlegte Methode aus. Hierzu wird das \gls{event} wie in Zeile 1 des Beispiels in runde Klammern
geschrieben und diesem die auszuführende Methode zugewiesen. Zuletzt ist das Zweiwege-Data-Binding
zu nennen, welches vornehmlich in Formularen Anwendung findet. Es ist in der Lage einen Wert einer
Komponente darzustellen, diesen aber auch zu aktualisieren, sofern der angezeigte Wert durch den
Nutzer geändert wird. Notiert wird dies durch \inlinecode{[(ngModel)]=wert} wie in Zeile 3 des
Beispiels dargestellt.

\begin{lstlisting}[float, caption=Data-Binding in Angular, label=code:dataBindingAngular, escapechar=ä]
<p ä\textbf{(click)="}\textbf{selectBsp(beispiel)"}>\textbf{\{\{beispiel.name\}\}}ä</p>
<beispiel-detail ä\textbf{[beispiel]="}\textbf{selectedBeispiel"}ä></beispiel-detail>
<input ä\textbf{[(ngModel)]="beispiel.name"}ä>
\end{lstlisting}

Zusätzlich können Interpolationen durch sogenannte Pipes bearbeitet werden, die beispielsweise ein
Datum oder eine Kreditkartennummer korrekt formatieren. Notiert werden diese wie folgt:
\inlinecode{\{\{wert | pipe\}\}}. Angular bietet einige vordefinierte Pipes, jedoch können auch
selbst neue Pipes definiert werden.

Direktiven sind eine weitere Möglichkeit, um Ausgaben im Template zu spezifizieren. Unterschieden
werden strukturelle Direktiven und Attribut-Direktiven. Erstere verändern das \ac{dom} durch
hinzufügen, entfernen oder ersetzen von Elementen. Code-Beispiel \ref{code:directivesAngular} zeigt
zwei mögliche Anwendungsfälle. Zum einen kann ein Ausdruck mittels \inlinecode{*ngFor} für jedes
Element einer Kollektion wiederholt werden und zum anderen mittels \inlinecode{*ngIf} nur unter
bestimmten Bedingungen gerendert werden. Attribut-Direktiven hingegen verändern bestehende Elemente
in ihren Eigenschaften und fügen sich in das Template wie gewöhnliche \ac{html}-Attribute ein.
Ebenso wie Pipes können auch Direktiven selbst definiert werden.

\begin{lstlisting}[float, caption=strukturelle Direktiven in Angular, label=code:directivesAngular]
<li *ngFor="let element of elemente"></li>
<beispiel-detail *ngIf="selectedBeispiel"></beispiel-detail>
\end{lstlisting}

Damit die definierten Templates mit einer Komponente zusammengeführt werden können, muss die
Komponente wie in Code-Beispiel \ref{code:componentAngular} mit dem
\textbf{@}\inlinecode{Component}-\gls{decorator} ausgestattet sein. Der \glspl{decorator} besitzt
unter anderem den Parameter \inlinecode{selector}, mittels dessen der Name definiert wird, unter dem
die View als Tag in andere Views eingebettet werden kann. Hinzu kommt der Parameter
\inlinecode{templateUrl}, der den Pfad zum zugehörigen Template definiert und zuletzt
\inlinecode{providers}, der die selbe Funktion wie bei der Moduldefinition erfüllt.

\begin{lstlisting}[float, caption=Komponente in Angular, label=code:componentAngular]
@Component({
	selector:    'app-my-list',
	templateUrl: './my-list.component.html',
	providers:  [ MyService ]
})
export class MyListComponent {
	// ...
}
\end{lstlisting}

Auch Angular bietet diverse \glspl{lifecycleCallback}, die jedoch hier nicht weiter im Detail
erläutert werden sollen, da sie im Wesentlichen den gleichen Umfang abdecken wie die
\glspl{lifecycleCallback} in React. Gleiches gilt für das im folgenden Abschnitt beschriebene
Framework Vue.js.
