\section{Komponenten in der Softwareentwicklung}
\label{sec:komponentenInDerSoftwareentwicklung}
Zu Beginn dieses Themas sollte zu aller erst definiert werden, was eine Komponente ist. Laut Duden
ist eine Komponente ein \glqq Bestandteil\grqq{} oder ein \glqq Element eines Ganzen\grqq{}
\cite[]{dudenKomponente}. Diese Definition lässt viel Interpretationsspielraum darüber, was in der
Softwareentwicklung als Komponente betrachtet werden kann. Möglich wäre somit die Bezeichnung einer
Datei, Klasse oder gar Funktion/Methode als Komponente.

Die in der Softwareentwicklung vorherrschenden Definitionen über den Begriff der Komponente sind
nicht einheitlich und ebenfalls sehr unscharf, beinhalten aber nach \cite[S.
305]{schattenBestPracticeKomponenten} im Wesentlichen folgende Punkte:

\begin{itemize}
	\label{enum:komponentenanforderungen}
	\item Eine Komponente verfügt über eine klare und stabile Schnittstelle.
	\item Eine Komponente bietet meist Funktionen höherer Abstraktion als eine einzelne
	Klasse\footnote{oder das jeweilige Äquivalent der gewählten Programmiersprache} an.
	\item Eine Komponente ist auf Wiederverwendbarkeit (in anderen Programmen oder Programmteilen)
	ausgelegt.
	\item Eine Komponente ist mit anderen Programmteilen lediglich locker verbunden und kann leicht
	ausgetauscht werden.
\end{itemize}

\cite{turowskiEnzWiInfSoftwarekomponenten} geht mit seiner Definition noch einen Schritt
weiter: \glqq Eine Softwarekomponente besteht aus verschiedenartigen (Software-)Artefakten. Sie ist
wiederverwendbar, abgeschlossen und vermarktbar, stellt Dienste über wohldefinierte Schnittstellen
zur Verfügung, verbirgt ihre Realisierung und kann in Kombination mit anderen Komponenten eingesetzt
werden, die zur Zeit der Entwicklung nicht unbedingt vorhersehbar ist.\grqq{}
\cite[]{turowskiEnzWiInfSoftwarekomponenten} Hinzu kommen in dieser Definition die Verbergung der
Realisierung und die Vermarktbarkeit der Komponente.

Der Ursprung dieser Definitionen liegt bereits im Jahr 1968 in der dort veranstalteten \glqq NATO
Software Engineering Conference\grqq{}, auf der Malcolm Douglas McIlroy einen Vortrag über \glqq
Mass Produced Software Components\grqq{} hielt und somit den Begriff des \glqq Component-based
software engineering\grqq{} prägte \cite[S. 138 ff.]{natoSoftwEngConf1968}. Knapp 30 Jahre später
wurde im Rahmen eines Workshops auf der European Conference on Object-Oriented Programming im Jahr
1996 unter anderem von Clemens Szyperski, der heute vielfach als Mitbegründer der
komponentenorientierten Software gilt, folgende Definition veröffentlicht, die heute als
ursprüngliche Definition dieser Disziplin gilt:

\begin{quote}
	\glqq A software component is a unit of composition with contractually specified interfaces and
	explicit context dependencies only. A software component can be deployed independently and is
	subject to composition by third parties.\grqq{} \cite[S. 41]{szyperskiComponentSoftware}
\end{quote}

Diese Definition enthält bereits die Merkmale der vertraglich spezifizierten Schnittstellen, der
losen Kopplung durch unabhängige Auslieferbarkeit und Möglichkeit zur Wiederverwendung in anderen
Programmen. \cite[]{szyperskiComponentSoftware}

Dass die Aufteilung von Quellcode nach bestimmten Gesichtspunkten zu einem definierten Zweck
unabhängig der Komponentendefinition keine neue Erfindung ist und war, zeigt ebenfalls ein Blick in
die Geschichte, die parallele Entwicklungen aufweist. So wurde in den 80er Jahren des 20.
Jahrhunderts mit dem Ziel separat kompilierbare Softwareteile zu erhalten, vermehrt auf modulare
Softwareentwicklung gesetzt. Ein Modul ist hierbei als \glqq austauschbares, komplexes Element
innerhalb eines Gesamtsystems, eines Gerätes o. Ä., das eine geschlossene [Funktions]einheit
bildet\grqq{} \cite[]{dudenModul} definiert. Das Modul ist also entgegen der oben genannten
Definition nach \citeauthor{schattenBestPracticeKomponenten} deutlich stärker an das System gebunden
als es von einer Softwarekomponente verlangt wird.

Nach dem Fokus auf Objektorientierung in den 90er Jahren wurde ab Anfang dieses Jahrtausends mit dem
Ziel der Wiederverwendung vermehrt auf Komponenten gesetzt. Dies schließt auch die Nutzung externer
Komponenten ein, die für ein Projekt hinzugekauft werden können \cite[S. 43]{matevskaSoftw1V01}. An
diesem Punkt greift auch die bereits von \cite{turowskiEnzWiInfSoftwarekomponenten} genannte
Definition, die insbesondere für den Zukauf von Komponenten relevant ist, da die Art der
Realisierung in diesem Zusammenhang keine Rolle spielen darf und die Vermarktbarkeit mit hoher
Kohäsion und klar definierten und stabilen Schnittstellen gleichzustellen ist.

\begin{figure}[htb]
	\centering
	\includegraphics{img/Stufen_der_Entkopplung.png}
	\citefigure{\cite[S. 302]{schattenBestPracticeKomponenten}}
	\caption[Stufen der Entkopplung]{Stufen der Entkopplung\footnotemark}
	\label{fig:entkopplung}
\end{figure}
\footnotetext{Diese Abbildung bezieht sich stark auf objektorientierte Programmierung. Eine Übertragung in andere Bereiche ist aber durchaus möglich.}

Betrachtet man Abbildung \ref{fig:entkopplung}, so ist erkennbar, dass sich Komponenten erst in den
letzten zwei Stufen der Entkopplung nach \citeauthor{schattenBestPracticeKomponenten} wiederfinden.
In Stufe eins sind, abgesehen von der Separierung einzelner Programmteile nach Zuständigkeit, keine
nennenswerten Maßnahmen zur Entkopplung getroffen. In dieser Stufe existieren ausschließliche
direkte Referenzen zwischen den Programmteilen, was eine hohe Kopplung und geringe
Wiederverwendbarkeit zur Folge hat. Ein erster Schritt zur Entkopplung führt über die Definition von
abstrakten Schnittstellen (Interfaces), hinter denen sich verschiedene Implementierungen verbergen
können. Diese Art der Softwareentwicklung ist in der objektorientierten Entwicklung ohne
Zuhilfenahme weiterer Frameworks vorherrschend.

Wird die Entkopplung weiter vorangetrieben, so ist die nächste Stufe meist die Nutzung eines
Komponentenframeworks. Dieses hat vereinfacht gesagt die Aufgabe, die Verbindung zwischen den
einzelnen Komponenten herzustellen und oft auch deren Konfiguration vorzunehmen. In dieser Stufe der
Entkopplung kann innerhalb einer Plattform komponentenorientiert entwickelt werden, da Frameworks in
den meisten Fällen auf eine Plattform beschränkt sind, wie zum Beispiel das Framework \glqq
Spring\grqq{} für Java.

Sollen die einzelnen Komponenten auf verschiedenen Plattformen entwickelt werden, bedarf es in der
Regel einer weiteren Entkopplungsstufe. In dieser Stufe ist nun weniger von Komponenten, als von
Services die Rede. Von einem Service wird im Gegensatz zur Komponente verlangt, dass seine
Schnittstelle plattformunabhängig beschrieben ist und dass dieser über ein Netzwerk für beliebige
Plattformen angeboten wird. Hierzu ist ein plattformunabhängiges Kommunikationsprotokoll wie
\ac{rest} oder \ac{soap} notwendig.

Das Ziel der immer weitergehenden Entkopplung wird verfolgt, um Teile eines Programms leicht
gegeneinander austauschen zu können. Auch werden durch die Entkopplung von Elementen Anwendungen
möglich, die über Systemgrenzen hinweg kommunizieren können. Dennoch ist bei all dem nicht außer
Acht zu lassen, dass mit der zunehmenden Abstraktion, die die Komponentenbildung mit sich bringt,
oft auch die Komplexität des Programmcodes zunimmt. Dies soll insbesondere in Abbildung
\ref{fig:entkopplung} durch den rechten unbeschrifteten Keil dargestellt werden. Hinzu kommt
außerdem bei der Verwendung von Frameworks die Einarbeitung in diese, deren Aufwand nicht zu
vernachlässigen ist. \cite[S. 302 ff.]{schattenBestPracticeKomponenten}
