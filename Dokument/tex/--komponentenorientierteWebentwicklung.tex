\section{Komponentenorientierte Webentwicklung}
\label{sec:komponentenorientierteWebentwicklung}
Im vorherigen Abschnitt wurde auf Komponenten in der Softwareentwicklung allgemein eingegangen.
Wendet man sich nun der Webentwicklung zu, fallen einige Besonderheiten auf und es sind einige
Abgrenzungen vorzunehmen.

Wird im Folgenden von Webentwicklung gesprochen, so ist nicht die Entwicklung reiner Business-Logik
in Form eines Backends für eine vollständige Webanwendung gemeint. Jedoch erfolgt diese im
Wesentlichen nach den Grundsätzen aus dem vorherigen Abschnitt. Vielmehr soll die Entwicklung des
Frontends samt Client-Logik betrachtet werden. Da es inzwischen möglich ist, eine Anwendung nahezu
vollständig auf dem Client zu programmieren und dementsprechend auch ein Großteil der Business-Logik
auf dem Client liegt, müssen die hier betrachteten Komponenten als Einheit aus Darstellung und
zugehöriger Logik betrachtet werden.

Die auf Seite \pageref{enum:komponentenanforderungen} genannte stichpunktartige Definition einer
Komponente kann weitestgehend für die Webentwicklung übernommen werden. Dennoch muss beachtet
werden, dass in dem Zusammenhang, in dem diese Definition aufgestellt wurde, primär die reine
Logik-Programmierung beachtet wurde. In der Webentwicklung kommt der Aspekt der Darstellung von
Inhalten hinzu, der eine neue Interpretation dieser Definition erfordert: Die Abstraktion bezieht
sich nun nicht mehr auf Klassen, sondern vielmehr auf Elemente des \ac{gui}.

Webentwicklung auf Basis von Komponenten ist inzwischen Alltag für die meisten Webentwickler. Die
drei am weitesten verbreiteten JavaScript-Frameworks, React\footnote{React ist eigentlich nur eine
Library, wird aber gerne im gleichen Atemzug mit den genannten und anderen Frameworks erwähnt.
\cite[]{goelHackrBestWebFrameworks}}, Angular und Vue.js, basieren allesamt auf Komponenten, wenn
auch die Umsetzung unterschiedlich ist.

Der Grund für die inzwischen weit verbreitete Verwendung von Komponenten in der Webentwicklung liegt
in der mit der Zeit gestiegenen Komplexität der Anwendungen, die zu entwickeln sind. Während in den
Anfangszeiten des Internets überwiegend einfache Websites zu entwickeln waren, beherrschen heute
komplexe Webanwendungen das World Wide Web. Um dieser Komplexität Herr zu werden, geht man heute
-- gemäß dem Motto \glqq teile und herrsche\grqq{} -- den Weg, komplexe Anwendungen aus einfachen
Komponenten zu konstruieren, die miteinander kommunizieren
\cite[]{bachUnicFrontendEntwicklerVergangenheitGegenwartZukunft}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\textwidth]{img/Komposition.jpg}
	\caption{Zerlegung und Komposition einer Anwendung}
	\label{fig:komposition}
\end{figure}

Die Entwicklung einer solchen Webanwendung erfolgt in vielen Fällen sowohl nach dem
Top-Down-Prinzip als auch nach dem Bottom-Up-Prinzip. Diese Prinzipien beschreiben Verfahren, nach
denen entweder eine große Einheit in immer kleinere Einheiten zwecks Komplexitätsreduktion zerteilt
wird (Top-Down) oder aber aus kleinen bereits existierenden Einheiten größere, komplexe Einheiten
komponiert werden (Bottom-Up). Die ganze Anwendung wird in der Regel nach dem Top-Down-Prinzip in
einzelne Funktionseinheiten, die im Folgenden als Organe bezeichnet werden sollen, zerlegt. Die
Organe wiederum bestehen aus Komponenten, die im Folgenden als Moleküle bezeichnet werden sollen.
Dies geschieht meist ebenfalls nach dem Top-Down-Prinzip, in einzelnen Fällen aber auch nach dem
Bottom-Up-Prinzip, sofern Moleküle existieren, die die gewünschte Funktionalität erfüllen. Moleküle
wiederum bestehen aus Grundkomponenten, die als Atome bezeichnet werden sollen. Diese werden oft
nach dem Bottom-Up-Prinzip zu Molekülen komponiert, in einigen Fällen jedoch auch nach dem
Top-Down-Prinzip selbst entwickelt. Abbildung \ref{fig:komposition} soll dies verdeutlichen. Möglich
wird dies durch Bibliotheken fertiger Komponenten, die in diversen Formen existieren.

Ein Beispiel eines solchen Prozesses könnte wie folgt aussehen: Eine beliebige Anwendung soll über
einen Informationsbereich (Organ) verfügen, der ein Kontaktformular (Molekül) enthalten soll. Dieses
Formular wird aus Formularfeldern (Atome) einer beliebigen Bibliothek erstellt.

Die Aufteilung in diesen Stufen soll primär als Schema und nicht als Entwicklungsrichtlinie dienen,
da eine so flache Komponenten-Hierarchie, wie in diesem Beispiel, in der Regel nicht erstrebenswert
ist. In der Praxis existieren diverse Zwischenstufen, die eine größtmögliche Wiederverwendbarkeit
der Komponenten, aber auch Wartbarkeit dieser ermöglichen sollen.
