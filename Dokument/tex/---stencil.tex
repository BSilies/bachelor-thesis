\subsection{Stencil}
Stencil ist wie bereits angekündigt ebenfalls kein Framework, sondern in erster Linie ein Compiler,
der Webcomponents beziehungsweise auf Webcomponents basierende und mit einer schlanken
Laufzeitumgebung ausgestattete Anwendungen generiert. Stencil beschreibt sich selbst als Kombination
der besten Konzepte der populärsten Frameworks und nutzt unter anderem TypeScript, \ac{jsx}, ein
virtuelles \ac{dom} und reactive (reaktives) Data-Binding. Entwickelt wurde Stencil vor allem aus
Performance-Gründen für eigene Zwecke von Ionic. Hinzu kam der Wunsch eine Möglichkeit dafür zu
schaffen, Komponenten entwickeln zu können, die in beliebigen Frameworks oder ganz unabhängig von
diesen eingesetzt werden können.\cite[]{stencilIntroduction}

Ziel von Stencil ist es Komponenten entwickeln zu können, die den Webstandards entsprechen und somit
den Entwicklern die Möglichkeit zu bieten auf ein Standard-\ac{api} zurückgreifen zu können statt
auf volatile \acp{api} von Frameworks angewiesen zu sein. Darüber hinaus bietet Stencil automatische
Optimierung des Codes und bezeichnet sich selbst als Zukunftssicher, da für fortschreitende
Entwicklungen von Standards und \acp{api} nicht der Quellcode, sondern in erster Linie der Compiler
angepasst werden muss. Dem Entwickler wird an dieser Stelle Arbeit abgenommen und Unterstützung
geboten, falls Änderungen notwendig sind. Die geforderte hohe Performance erreicht Stencil durch
Nutzung der Browser-\acp{api} statt diese Funktionalität selbst zu implementieren. Somit ist das
Stencil-\ac{api} vergleichsweise schlank, da auch die Entwickler angehalten sind die
Browser-\acp{api} zu nutzen. Gleichzeitig erleichtert dies den Einstieg in Stencil, da der
Lernaufwand verhältnismäßig gering ist. Obwohl Stencil sich selbst ausdrücklich nicht als Framework
bezeichnet, möchte es dennoch die Möglichkeiten eines Frameworks für die Entwicklung bieten, aber
dennoch nah an den Web-Standards operieren. \cite[]{stencilGoalsObjectives}

Um eine Komponente in Stencil zu definieren, bedarf es einer JavaScript-Klasse wie es auch im Custom
Elements-Standard vorgesehen ist. Zur Definition als Komponente wird diese mit dem
\textbf{@}\inlinecode{Component}-\gls{decorator} versehen, wie es Code-Beispiel
\ref{code:stencilComponent} zeigt. Dieser wird mit einem Options-Objekt versehen, dessen einziges
verpflichtender Wert \inlinecode{tag} ist, der den Namen des \ac{html}-Elements definiert, mit dem
die Komponente eingebunden werden kann. Weitere Parameter können zum Beispiel ein zugehöriges
Stylesheet oder in der Komponente benötigte Ressourcen definieren. Darüber hinaus muss, sofern ein
Shadow \ac{dom} zur Kapselung gewünscht ist, das optionale Element \inlinecode{shadow} auf
\inlinecode{true} gesetzt werden. Da sämtliche Komponenten in Stencil mittels TypeScript entwickelt
werden und \ac{jsx} nutzen, ist die Dateiendung einer Komponente in jedem Fall \inlinecode{.tsx}.
\cite[]{stencilComponent}

\begin{lstlisting}[float, caption=Komponentendefinition in Stencil, label=code:stencilComponent]
@Component({tag: 'my-component', styleUrl: 'my-component.css'}) export class MyComponent {}
\end{lstlisting}

Stencil bietet eine Vielzahl an \glspl{lifecycleCallback}, die zum Teil Ähnlichkeiten mit denen aus
React aufweisen. Übernommen aus dem Custom Elements-Standard wurden der
\inlinecode{connectedCallback} sowie der \inlinecode{disconnectedCallback}. Neu sind Methoden, die
jeweils vor und nach den Ereignissen Laden, Rendern und Aktualisieren aufgerufen werden, jedoch
nicht weiter im Detail betrachtet werden sollen. \cite[]{stencilLifecycle}

Stencil unterscheidet zwischen Properties und State und tut es somit React gleich. Als Properties
werden die Eingabeparameter einer Komponente bezeichnet, die ein zugehöriges Attribut besitzen,
während der State den internen Zustand der Komponente wiederspiegelt. Properties werden mit dem
\textbf{@}\inlinecode{Prop}-\gls{decorator} versehen und erhalten entweder explizit oder implizit
durch den vordefinierten Wert einen Datentypen, wie es Code-Beispiel \ref{code:stencilProps} zeigt.
Dass ein Datentyp zugewiesen werden kann, wird durch die eingangs erwähnte Nutzung von TypeScript
ermöglicht, die ebenso für die Nutzbarkeit der \gls{decorator} verantwortlich ist. Der
\textbf{@}\inlinecode{Prop}-\gls{decorator} kann ein Options-Objekt aufnehmen, mittels dessen ein
abweichender Attributname festgelegt sowie die Unveränderbarkeit der Properties aufgehoben werden
kann. Darüber hinaus kann festgelegt werden, dass der Wert der Property auf das zugehörige Attribut
reflektiert werden soll. \cite[]{stencilProp}

\begin{lstlisting}[float, caption=Properties in Stencil, label=code:stencilProps]
export class MyComponent {@Prop() stringProp: string; @Prop() numberProp: number; @Prop()
	booleanProp: boolean; @Prop() objectProp: Object;}
\end{lstlisting}

Der \textbf{@}\inlinecode{State}-\gls{decorator} wird genutzt, um interne Daten zu kennzeichnen, die
bei einer Veränderung ein erneutes Rendern der Komponente auslösen sollen. Ein Zugriff auf den State
von außerhalb der Komponente ist nicht vorgesehen, wodurch der State das Gegenstück zu den Props
bildet.
\cite[]{stencilState}

Änderungen der Properties führen in Stencil zu einem erneuten Rendering der Komponente, sofern dies
nötig sein sollte. Um Validierungen oder Seiteneffekte bei Änderung einer Property durchführen zu
können, hat Stencil den \inlinecode{attributeChangedCallback} durch einen
\textbf{@}\inlinecode{Watch}-decorator ersetzt. Mit diesem werden Methoden versehen, die bei
Änderung einer Property ausgeführt werden sollen. Dieser bekommt als einzigen Parameter den Namen
der Property als String übergeben, auf deren Änderung reagiert werden soll. \cite[]{stencilWatch}

Die Gestaltung des Templates erfolgt mittels dem aus React bekannten \ac{jsx}. Die
\inlinecode{render}-Funktion einer Komponente gibt hierbei das in \ac{jsx} geschriebene Template
zurück. Da \ac{jsx} bereits aus Kapitel \ref{ssec:react} bekannt ist, soll an dieser Stelle nur
darauf hingewiesen werden, dass in Stencil auch Slots nutzbar sind, wie sie in der \ac{html}
Templates-Spezifikation definiert wurden, um der Komponente dynamisch Kind-Elemente hinzuzufügen.
\cite[]{stencilJsx}

In der Regel sollten in Stencil \ac{dom}-\glspl{event} genutzt werden, da kein eigenes
\glspl{event}-\ac{api} bereitgestellt wird. Stencil unterstützt in der Entwicklung jedoch über ein
\ac{api}, welches zwei \glspl{decorator} bereitstellt, um \glspl{event} auszulösen oder die
Komponente auf \glspl{event} reagieren zu lassen. Für ersteres existiert der
\textbf{@}\inlinecode{Event}-\gls{decorator}, dem eine Variable vom Typ \inlinecode{EventEmitter<T>}
folgt. Der Typ-Parameter nimmt in diesem Fall den Typ der zu übergebenden Daten auf. Standardmäßig
ist der Eventname der Name der Variable, jedoch kann dies über ein Options-Objekt geändert werden.
Dies gilt ebenso für die Einstellungen, ob das \glspl{event} im \ac{dom} aufsteigt (bubbles),
stornierbar (cancelable) ist oder die Grenze zwischen Shadow \ac{dom} und dem regulären \ac{dom}
überwinden kann (composed). Code-Beispiel \ref{code:stencilEmitEvent} zeigt ein so definiertes
\gls{event}, sowie eine Funktion, die dieses mittels \inlinecode{emit} auslöst.

\begin{lstlisting}[float, caption=Events in Stencil auslösen, label=code:stencilEmitEvent]
export class MyComponent {@Event({eventName: 'my-event', composed: true, cancelable: true, bubbles:
	true,}) myEvent: EventEmitter<string>;

	somethingHappenedHandler(text: string) {this.myEvent.emit(text);}}
\end{lstlisting}

Mittels des \textbf{@}\inlinecode{Listen}-\gls{decorator}s kann einer Komponente ein
\gls{eventListener} hinzugefügt werden. Dieser wird der Funktion vorangestellt, die als Reaktion auf
das \gls{event} ausführt werden soll. Der \gls{decorator} kann zwei Parameter entgegennehmen, von
denen der erste verpflichtend gesetzt werden muss und den \gls{event}-Namen als String enthält. Der
zweite Parameter ist auch hier wieder ein Options-Objekt, mit dem zum einen ein anderes Ziel
ausgewählt werden kann, an das der Listener angehängt werden soll. Zum anderen kann festgelegt
werden, dass der Listener in der capture-Phase eines Events ausgelöst werden soll oder dieser
passiv ist. \cite[]{stencilEvents}

Als weiteres Feature bietet Stencil das \inlinecode{<Host>}-Element, welches in der
\inlinecode{render}-Funktion einer Komponente genutzt werden kann. Dieses Element ist ein virtuelles
Element, das dem Entwickler erlaubt, Attribute und Event-Listener an das Host-Element der Komponente
anzuhängen. In \ac{jsx} kann dieses wie jedes andere Element genutzt werden. Die einzige
Einschränkung ist, dass es die Wurzel in der \inlinecode{render}-Funktion bilden muss. Dieses
Element dient statt dem Rendering des Host-Elements ausschließlich der deklarativen Konfiguration
von diesem.

Wird eine Referenz auf das Host-Element benötigt, ist dies mittels des
\textbf{@}\inlinecode{Element}-\gls{decorator}s möglich. Wird dieser einer Variable vom Typ
\inlinecode{HTMLElement} vorangestellt, so enthält diese die benötigte Referenz und kann fortan
genutzt werden, um Standard \ac{dom}-Operationen auf diesem auszuführen. \cite[]{stencilHostElement}

Das Styling in Stencil erfolgt nach den gleichen Prinzipien, wie sie in den Webstandards beschrieben
sind; jedoch in einer separaten Datei. Werden die Komponenten so konfiguriert, dass sie ein Shadow
\ac{dom} besitzen, so gelten die ihnen zugewiesenen Styles ausschließlich für die Komponente. Auch
wenn Stencil die Entwickler ermutigt Styles ausschließlich auf Komponenten-Ebene zu definieren, so
wird dennoch die Möglichkeit geboten globale Styles zu definieren, die insbesondere für die
Definition von \ac{css}-Variablen, Fonts oder Seitenhintergründen sinnvoll sein können.
\cite[]{stencilStyling}

Neben all diesen Punkten bietet Stencil einen Initializer und ein eigenes \ac{cli}. Mittels des
Initializers, der drei Optionen zur Auswahl bietet, kann ein kompilierbares Startprojekt erstellt
werden, das in der folgenden Entwicklung angepasst werden kann. Die Auswahlmöglichkeiten sind \glqq
ionic-pwa\grqq{}, \glqq app\grqq{} und \glqq component\grqq{}. App wird genutzt, um ein Projekt zu
erstellen, das eine in sich geschlossene Anwendung oder Website enthalten soll, während component
genutzt wird, um eine Sammlung von Komponenten zu erstellen, die beliebig eingesetzt werden können.
Ionic-pwa dient zur Erstellung einer Progressive Web App, also einfach gesagt eine Webapplikation
die bestimmte Funktionalitäten einer nativen App bietet, worauf in diesem Rahmen jedoch nicht weiter
eingegangen werden soll.\cite[]{stencilGettingStarted}

Zusätzlich bietet Stencil die Möglichkeit drei verschiedene Versionen einer Anwendung zu erzeugen.
Standardmäßig erzeugt der Compiler eine komplette Anwendung, die in der Form auf einem Webserver
lauffähig wäre. Jedoch kann der Compiler ebenso die Komponenten für sich erzeugen und zwar zum
einen als reine Webcomponents, zum anderen aber auch als Bibliothek, die die Komponenten dynamisch
in die Anwendung laden kann. Hinzu kommt, dass in der ersten und letzten genannten Version neben dem
Code für moderne Browser zusätzlich eine Variante erzeugt wird, die ältere Browser unterstützt und
dem ECMAScript 5-Standard entspricht. Soll die letztgenannte Komponentenbibliothek genutzt werden,
so kann über die in Code-Beispiel \ref{code:stencilIntegration} gezeigten Imports gewährleistet
werden, dass jedem Browser nur die Variante geliefert wird, die er verarbeiten kann.

\begin{lstlisting}[float, caption=Stencil Integration, label=code:stencilIntegration]
<script type="module" src="/build/app.esm.js"></script>
<script nomodule src="/build/app.js"></script>
\end{lstlisting}

Das \ac{cli} kann in einigen Punkten bei der Entwicklung unterstützen, von denen an dieser Stelle
allerdings nur einer genannt werden soll. So kann mittels des Kommandos \inlinecode{generate} eine
neue Komponente inklusive zugehörigem Stylesheet und Tests erstellt werden. Auf die Tests soll
jedoch in diesem Kontext nicht weiter eingegangen werden, da die reine Existenz dieser für den
anstehenden Vergleich ausreichend ist. \cite[]{stencilFirstComponent}
