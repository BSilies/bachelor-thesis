\section[Vergleich von Webcomponents und Framework-Komponenten]{Vergleich von Webcomponents und\\Framework-Komponenten}
\label{sec:vergleichWebcomponentsFrameworkKomponenten}
Um Webcomponents und die Komponenten der zuvor beschriebenen Frameworks strukturiert miteinander
vergleichen zu können, bedarf es einiger Vergleichskategorien. Folgende Leitfragen sollen dies
erfüllen und als Basis für den Vergleich dienen.

\begin{samepage}
	\begin{itemize}
		\item Welche spezifischen Merkmale haben die Frameworks?
		\item Wie leicht ist das Framework zu erlernen?
		\item Welche Schnittstellen haben die Komponenten und wie sind diese Schnittstellen
		aufgebaut?
		\item Wie setzen die Komponenten Kapselung um?
		\item Wie lassen sich die Frameworks/Webcomponents in bestehende Anwendungen integrieren und
		ist eine Integration untereinander möglich?
		\item Wie performant sind die genannten Frameworks im Vergleich untereinander und zu
		Webcomponents?
	\end{itemize}
\end{samepage}

Betrachtet werden soll hier auch nur die Entwicklung von Webcomponents mittels der in Kapitel
\ref{sec:webcomponents} beschriebenen Standards ohne Zuhilfenahme von Frameworks. Ein Vergleich von
Frameworks zur Entwicklung von Webcomponents folgt in Kapitel
\ref{sec:vergleichderBeschriebenenFrameworks}.

Zu Anfang soll ein Vergleich anhand spezifischer Merkmale erfolgen. Zuerst ist festzustellen, dass
es sich bei Vue und Angular um JavaScript-Frameworks handelt, während React lediglich eine
Bibliothek ist. Webcomponents basieren auf den Webstandards zu Custom Elements, Shadow \ac{dom} und
\ac{html} Templates, bedürfen also keiner externen Ressourcen. Ein spezifisches Merkmal von React
ist, dass es ebenso wie auch Vue auf einem virtuellen \ac{dom} operiert und dieses mit dem realen
\ac{dom} synchronisiert. Webcomponents sowie Angular besitzen kein virtuelles \ac{dom}. Vue und
Webcomponents können ausschließlich mittels \ac{html}, \ac{css} und Java\-Script entwickelt werden.
Für React wird empfohlen die JavaScript-Erweiterung \ac{jsx} zu nutzen und Angular empfiehlt
ausdrücklich die Anwendungen mittels Type\-Script zu entwickeln. Die Verwendung von TypeScript ist in
den anderen Frameworks ebenso möglich, jedoch nicht notwendig. Angular- und Vue-Komponenten werden
über eine Template-Datei gestaltet. Dies ist auch für Webcomponents möglich, jedoch können diese
auch mittels des \ac{dom}-\ac{api}s gestaltet werden. In React wird das Markup zusammen mit der
Logik geschrieben, was durch \ac{jsx} ermöglicht wird. Somit ist für React kein separates Template
notwendig und eine Komponente besteht in der Regel ausschließlich aus einer Datei\footnote{React
schreibt nicht vor, wie das Styling erfolgt. Dies wird über Erweiterungen ermöglicht, oder durch ein
globales Stylesheet, welches in der \inlinecode{index.html} importiert wird.}. Angular und Vue
besitzen ein Data-Binding-Konzept, mittels dessen Daten der Komponente in das Template geschrieben
und aus dem Template gelesen werden können. Für React ist dies nicht notwendig, da auch das Markup
durch JavaScript in Form von \ac{jsx} geschrieben wird und somit Daten durch Reactive Binding direkt
eingepflegt werden können. Die Webcomponents-Standards bieten keine dieser Möglichkeiten, sodass es
Aufgabe des Entwicklers ist Daten über das \ac{dom}-\ac{api} zu lesen und zu schreiben. Ebenso
verhält es sich mit Direktiven, die Angular und Vue bieten, in React überflüssig sind und für
Webcomponents kein Konzept dieser Art existiert. Vue liefert neben dem Rendering-Kern Bibliotheken
für Routing und State-Management innerhalb einer Webanwendung mit. State-Management ist ein
zentrales Thema von React und wird ebenfalls im Kern mit geboten; Routing und weitere
Funktionalitäten müssen jedoch über externe Bibliotheken eingebunden werden. Angular hingegen bietet
von Haus aus einen Router an, jedoch muss das State-Handling über externe Bibliotheken erfolgen.
Webcomponents bieten dies nicht, da der Fokus auch ein anderer ist. Die Webcomponents-Standards
wurden geschaffen, um wiederverwendbare Bausteine zu schaffen. Die genannten Frameworks hingegen
haben das Ziel komplexe Webanwendungen zu ermöglichen und bedürfen somit erweiterter Funktionalität.

Das Thema Erlernbarkeit auf einem grundlegenden Niveau ist ohne große Unsicherheiten abhandelbar.
Webcomponents werden ausschließlich mittels \ac{html}, \ac{css} und JavaScript unter Zuhilfenahme
des \ac{dom}-\ac{api}s entwickelt. Da dies Grundlagen eines jeden Webentwicklers sein sollten und
die Standards ebenfalls verhältnismäßig schlank gehalten sind, ist ein Einstieg mit minimalem
Aufwand möglich. Jedoch ist die Gestaltung einer Komponente verhältnismäßig aufwendig und es liegen
viele Aufgaben in der Hand des Entwicklers. Vue wird als am einfachsten zu erlernendes der drei
beschriebenen Frameworks angesehen und steht aufgrund des reduzierten Entwicklungsaufwandes etwa
gleichauf mit der Entwicklung von Webcomponents. Dies ist darin begründet, dass ebenfalls lediglich
\ac{html}, \ac{css} und JavaScript benötigt werden und die Initialisierung eines Projektes ebenfalls
ohne großen Aufwand möglich ist. Auch die Template-Syntax und das Konzept kann mit geringem Aufwand
grundlegend erlernt werden. Anders wird dies erst, wenn komplexere Projekte in Angriff genommen
werden. React und Angular bedürfen einer komplexeren Projektinitialisierung und sind auch sonst im
Vergleich zu Vue komplexer, da für React die Template-Sprache \ac{jsx} erlernt werden muss und
Angular mittels TypeScript entwickelt wird. Auch die Konzepte hinter React und Angular müssen
erlernt werden, was sich jedoch bei Angular als echtem Framework im Vergleich zu React als
aufwändiger herausstellt. Je nach Vorlieben der Entwickler kommt bei React erschwerend hinzu, dass
Markup und Logik nicht getrennt werden, oder aber diese Trennung in Vue und Angular erforderlich
ist. Eine Festlegung welches der Frameworks komplexer ist, soll und kann an dieser Stelle nicht
getroffen werden. Soll der Funktionsumfang des gesamten Frameworks erlernt werden, so ist
wahrscheinlich Angular aufgrund der Masse an Funktionalitäten komplexer als React, dennoch bietet
auch React mit seinem State-Handling einige Komplexität.
\cite[]{academindAngularReactVue}

Die Schnittstellen der Komponenten sind bei allen Frameworks und den Webcomponents ähnlich. Genutzt
werden die Komponenten bei allen wie gewöhnliche \ac{html}-Elemente. Die Eingabeschnittstellen in
React und Vue werden als Props bezeichnet, in Angular als Input-Binding.
\cite[]{entwicklerDeSpringerJsFwsVergleich} Webcomponents besitzen zum einen JavaScript-Properties
und zum anderen \ac{html}-Attribute als Eingabemöglichkeiten. Der umgekehrte Datenfluss erfolgt in
allen Fällen über \glspl{event}.

Alle Frameworks sowie Webcomponents setzen die nach Komponentendefinition geforderte Kapselung um.
Die Komponenten aller Frameworks können seiteneffektfrei und ohne Kenntnis der inneren Struktur
eingesetzt werden. Erforderlich ist lediglich das Wissen über die Schnittstelle. Unterschiede
existieren im Umfang der Kapselung, insbesondere hinsichtlich des Stylings. Webcomponents kapseln
standardmäßig die für sie definierten Styles, sodass keine Wechselwirkungen mit den Styles anderer
Komponenten zu erwarten sind. Dasselbe kann in Vue erreicht werden, jedoch müssen die entsprechenden
Styles explizit als \inlinecode{scoped} deklariert werden. Angular bietet die Möglichkeit sowohl
globale als auch komponentenspezifische Styles zu definieren. React bietet diese Möglichkeit nur
durch zusätzliche Erweiterungen.

Das Thema Integration soll aus mehreren Blickwinkeln betrachtet werden. Zuerst ist festzuhalten,
dass Webcomponents ohne großen Aufwand in bestehende Anwendungen integriert werden können, da sie
auf Webstandards basieren. Vue und React lassen sich ebenfalls in bestehende Anwendungen
integrieren, indem einzelne Komponenten eingebunden werden. Lediglich die zugehörigen Bibliotheken
müssen von der Anwendung geladen werden. Angular hingegen bietet nicht die Möglichkeit sukzessive in
bestehende Anwendungen integriert zu werden.

Als weiterer Punkt soll die Integrierbarkeit der Frameworks und Webcomponents ineinander betrachtet
werden. Da Vue- und React-Komponenten lediglich ein Host-Element benötigen, an das sie angehängt
werden, ist es theoretisch\footnote{Praxiserfahrungen des \acs{lsg}-Projekts zeigen, dass es bei der
Entwicklung von Webcomponents mittels React Schwierigkeiten gibt.} ohne große Hindernisse möglich
Webcomponents mittels dieser Frameworks zu entwickeln. Mit Angular ist dies nicht ohne zusätzliche
Erweiterungen möglich, da Angular-Komponenten den Kontext einer Angular-Anwendung benötigen. Vue und
React können aus den genannten Gründen auch in einer Anwendung koexistieren, während dies mit
Angular nicht möglich ist. Dennoch können Webcomponents, Vue- und React-Komponenten in Angular
genutzt werden, wenn auch ein erhöhter Programmieraufwand nötig ist. Ebenso verhält es sich mit der
Nutzung von Komponenten fremder Frameworks in Vue und React. Insbesondere bei Vue und React ist zu
beachten, dass diese Frameworks auf einem virtuellen \ac{dom} basieren und somit in jedem Fall
erhöhter Programmieraufwand benötigt wird, um die fremden Komponenten mit dem virtuellen \ac{dom} in
Einklang zu bringen.

Die Performance der verschiedenen Entwicklungsarten kann ebenfalls aus verschiedenen
Perspektiven betrachtet werden. Zuerst spielt die Größe des Frameworks eine Rolle, da diese die
Ladezeit der Anwendung beeinflusst. Hier ist Vue mit etwa 80~kB knapp vor React mit 100~kB am
leichtgewichtigsten. Angular folgt mit einer Größe von etwa 500~kB, was allerdings durch den hohen
Funktionsumfang des Frameworks begründet ist. \cite[]{martinHackernoonAngularReactVue} Werden Vue
und React jedoch über Fremdbibliotheken erweitert, steigt auch hier die Größe. Die Zahlen zu Angular
stammen aus 2019 und aktuell werden seitens Angular viele Bemühungen unternommen das Framework zu
verschlanken, sodass dort inzwischen eine Annäherung stattgefunden haben dürfte.
\cite[]{academindAngularReactVue}

Ein viel zitierter, aber auch schon aus dem Jahr 2018 stammender Benchmark-Test von
\cite{krauseBenchmark} vergleicht diverse Frameworks hinsichtlich verschiedener
Performance-Kriterien. Die erste Kategorie untersucht die Laufzeitperformance der Frameworks nach
initialem Laden der Seite anhand von Tabellenoperationen. Mit Ausnahme eines Tests schneiden die
Webcomponents in dieser Kategorie durchgehend mit den niedrigsten Zeiten am Besten ab. Die einzige
Ausnahme bildet die Auswahl einer Tabellenzeile als Reaktion auf ein \gls{event}. Diese Aufgabe
lösen alle Frameworks in etwa mit der gleichen Geschwindigkeit, jedoch schneidet Angular, wenn auch
mit größerer Standardabweichung, am besten ab. Schwächen beweisen Angular und React vor allem bei
dem Tausch zweier Tabellenzeilen in einer Tabelle mit 1.000 Zeilen, wofür beide Frameworks in etwa
fünf mal so viel Zeit benötigen wie Webcomponents und Vue. Vue besitzt die größten Schwächen in
partiellen Updates, bei denen jede zehnte Tabellenzeile einer Tabelle mit 10.000 Zeilen verändert
wird. Grundsätzlich kann für diese Kategorie festgehalten werden, dass Vue für die gestellten
Aufgaben im Schnitt 1,3 mal so viel Zeit benötigt wie es Webcomponents tun und Angular sowie React
mit jeweils etwa 1,4 mal so viel Zeit folgen und nahezu gleich auf sind. Bei der Kategorie der
Start-Metriken stechen die Webcomponents ebenfalls alle anderen Frameworks aus. Besonders
bemerkenswert ist hierbei die benötigte Zeit, ehe alle Skripte vollständig geladen sind. Vue liegt
mit etwa dem 3,4-fachen der Zeit der Webcomponents auf Platz zwei vor React mit dem 4,1-fachen.
Angular ist mit der 14,7-fachen Zeit weit abgeschlagen. Diese Reihenfolge, wenn auch mit anderen
Faktoren, spiegelt sich auch in allen weiteren Tests dieser Kategorie wieder. Bei der Nutzung des
Arbeitsspeichers benötigen die Webcomponents ebenfalls durchgehend am wenigsten Ressourcen. Vue und
React liegen in etwa gleich auf und benötigen je nach Aufgabe etwa 1,4 bis 2,8 mal so viel Speicher
wie die Webcomponents. Angular benötigt mit dem 2,8 bis 3,6-fachen der Webcomponents am meisten
Speicher. \cite[]{krauseBenchmark}\footnote{Verglichen wurden: vanillajs-wc-keyed,
vue-v2.5.16-keyed, angular-v6.1.0-keyed, react-v16.4.1-keyed}

In einigen Punkten werden die Zahlen des Performance-Tests veraltet sein, insbesondere aufgrund der
bereits genannten Optimierung von Angular. Dennoch kann festgehalten werden, dass die Frameworks in
nahezu allen Punkten den Webcomponents unterlegen sind und Vue möglicherweise eine etwas bessere
Performance aufweist als seine konkurrierenden Frameworks. Als relativierender Faktor muss jedoch
benannt werden, dass bei diesem Test keine Polyfills für Webcomponents geladen wurden. In einem
realen Projekt, in dem diverse Browser unterstützt werden müssen, wären Webcomponents somit
möglicherweise den anderen Frameworks nicht mehr überlegen.
