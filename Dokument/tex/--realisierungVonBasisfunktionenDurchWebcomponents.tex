\section{Realisierung von Basisfunktionen durch Webcomponents}
Der Letzte Abschnitt dieses Kapitels soll sich mit den Basisfunktionen einer Website befassen und
wie diese durch Webcomponents umgesetzt werden können. Hierzu muss zuerst definiert werden, was
unter Basisfunktionen zu verstehen ist, da für diesen Kontext keine Definition dieses Begriffs
existiert.

Als Basisfunktionen sollen in diesem Zusammenhang Funktionalitäten bezeichnet werden, die sich
weniger mit der Darstellung von Inhalten als vielmehr mit der globalen Steuerung der Website
beziehungsweise Webanwendung befassen. Im Rahmen dieses Ausblicks sollen zwei dieser Funktionen
näher beleuchtet werden.

Eine Basisfunktion, jedoch nur von \acp{spa}, ist das Routing einer Seite. Hierunter ist die
bedingte Darstellung von Inhalten einer \ac{spa} in Abhängigkeit zum \ac{url} der Seite zu
verstehen. Während \acp{mpa} verschiedene Inhalte durch physisch getrennte Seiten präsentieren, wird
in einer \ac{spa} lediglich eine Seite an den Nutzer ausgeliefert, deren Inhalte sich jedoch
dynamisch anpassen. Somit wird ein ähnliches Verhalten wie das einer \ac{mpa} erreicht, jedoch mit
dem Vorteil, dass eine \ac{spa} oft eine bessere \ac{ux} aufweist, da kein wiederholtes Seitenladen
notwendig ist.

Im Internet sind diverse Anleitungen zu finden, die beschreiben, wie ein Router ohne die Nutzung von
Frameworks implementiert werden kann. Als Grundlage für die weitere Aufarbeitung soll ein Blog-Post
von \citeauthor{hackdoorCanellaVanillaJsRouter} dienen \cite[]{hackdoorCanellaVanillaJsRouter}.
Grundsätzlich lassen sich die Aufgaben eines Routers auf wenige Punkte beschränken:

\begin{itemize}
	\item Verwalten der Routen inklusive Verhalten bei Übereinstimmung mit dem aktuellen
	\ac{url}-Pfad.
	\item Ermitteln des aktuellen \ac{url}-Pfades.
	\item Setzen eines neuen \ac{url}-Pfades.
	\item Überwachen des \ac{url} auf Änderungen.
\end{itemize}

Die genannten Punkte in Form von Webcomponents zu implementieren, ist möglich, bedürfen in einigen
Punkten jedoch Änderungen gegenüber des verwendeten Beispiels, die im Folgenden theoretisch
dargelegt werden. Es werden ausdrücklich keine Implementierungsdetails genannt, da diese den Rahmen
dieses Kapitels sprengen würden.

Für die vollständige Implementierung eines Routers eignet sich ein Zwei-Komponenten-Ansatz. Zum
einen wird eine Router-Komponente benötigt, die die oben genannten Aufgaben implementiert, zum
anderen ist zur Definition der Routen eine Route-Komponente sinnvoll. Letztgenannte Komponente dient
hierbei als Rahmen für die Inhalte, die unter einer Route angezeigt werden sollen, die über eine
Property der Komponente definiert wird. Alle so definierten Routen werden wiederum von dem Router
eingeschlossen.

Der Router besitzt in der Implementierung, die als Vorlage dient, fünf Funktionen sowie weitere
Hilfsfunktionen, auf die jedoch in diesem Kontext nicht weiter eingegangen werden soll. Die
Funktionen \inlinecode{add}, \inlinecode{remove} und \inlinecode{flush} dienen dem Hinzufügen und
Entfernen von Routen, mittels \inlinecode{navigate} kann ein neuer \ac{url}-Pfad gesetzt werden und
\inlinecode{listen} reagiert auf Änderungen im \ac{url}-Pfad um den Zustand zu aktualisieren. Die
vier erstgenannten Funktionen werden in der genannten Implementierung von außerhalb der Klasse
angesprochen, sodass hierfür in der Webcomponents-Implementierung ein neuer Mechanismus geschaffen
werden muss.

Es bietet sich an, dass die Route-Komponenten ihre definierten Routen dem Router mittels des
\gls{lifecycleCallback}s \inlinecode{connectedCallback} selbst hinzufügen. Dies gilt ebenso für das
Entfernen der Routen im \inlinecode{disconnectedCallback}. Genutzt werden können hierzu
\glspl{event}, die in den Route-Komponenten ausgelöst werden und von der Router-Komponente behandelt
werden. Neue \ac{url}-Pfade können ebenfalls über \glspl{event} gesetzt werden, die in den
Funktionen ausgelöst werden, in denen eine Pfadänderung nötig wird. Die Behandlung erfolgt auch hier
im Router.

Die Route-Komponente übergibt dem Router bei der Definition einer neuen Route zusätzlich eine
\gls{callback}-Funktion, über die bei Übereinstimmung der Route mit dem aktuellen \ac{url}-Pfad das
weitere Verhalten definiert wird. In dieser Funktion kann somit der Zustand der Route-Komponente
dahingehend aktualisiert werden, dass diese die definierten Kind-Elemente anzeigt. Im Umkehrschluss
muss auch eine Möglichkeit geschaffen werden, dass die Route-Komponente darüber benachrichtigt wird,
sobald \ac{url}-Pfad und definierte Route nicht mehr übereinstimmen, um ihren Zustand dahingehend zu
aktualisieren, dass ihre Kind-Elemente nicht mehr angezeigt werden. Auch hierfür eignet sich eine
\gls{callback}-Funktion, die als Rückgabewert der ersten \gls{callback}-Funktion definiert wird und
vom Router ausgeführt wird, sobald eine Änderung des \ac{url}-Pfades festgestellt wird.

Der hier beschriebene Ansatz ist nur eine von vielen Möglichkeiten und bietet mit Sicherheit
weiteres Optimierungspotential. Ziel des Autors war es einen Ansatz zu entwickeln, in dem der Router
ausschließlich im Template einer Anwendung konfiguriert werden kann und kein weiterer
JavaScript-Code notwendig ist.  Sucht man im Internet nach Routern für Webcomponents, so finden sich
dort bereits diverse Implementierungen, die frei nutzbar sind und somit nicht zwingend eine eigene
Entwicklung notwendig ist.

Als weitere Basisfunktion kann das Tracking des Nutzungsverhaltens einer Seite mittels Angeboten wie
\glqq Google Analytics\grqq{} gesehen werden. Der Einsatz einer Webcomponent ist auch hier möglich,
bringt jedoch nur geringen Mehrwert. Die Aufgabe der Komponente wäre in erster Linie, den
Code-Schnipsel, der das Tracking initialisiert, zu kapseln und die Nutzerspezifischen Einstellungen
über Properties vorzunehmen. Darüber hinaus bieten diese Dienste oft auch die Möglichkeit
benutzerdefinierte Tracking-\glspl{event} zu übermitteln. Für diese Option bietet sich ein
eventgesteuertes Verhalten an, bei dem aus beliebigen Komponenten heraus \glspl{event} eines per
Vertrag definierten Typs gesendet werden können. Diese werden dann mittels eines
\gls{eventListener}s, der auf das \inlinecode{document}-Objekt der Seite registriert ist,
verarbeitet und die Ergebnisse an den Tracking-Dienst übermittelt.

Auch ein zentrales State-Handling ließe sich mit Webcomponents implementieren, doch ist hier die
Frage, ob dies nicht einen zu hohen Mehraufwand mit sich bringt. Die Folge dieses Ansatzes wäre,
dass sämtliche Zustandsänderungen über \glspl{event} angestoßen werden und der aktualisierte Zustand
den Komponenten über Properties mitgeteilt wird. Dieser Ansatz käme dem State-Handling von React
gleich, dennoch wäre wahrscheinlich ein klassenbasierter Ansatz in diesem Kontext mit weniger
Aufwand verbunden.
