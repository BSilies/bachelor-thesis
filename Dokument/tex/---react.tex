\subsection{React}
\label{ssec:react}
Wie bereits in Kapitel \ref{sec:komponentenorientierteWebentwicklung} angemerkt, handelt es sich bei
React nicht um ein Framework, sondern lediglich um eine JavaScript-Bibliothek. Nichtsdestotrotz soll
React in dieser Arbeit in den Vergleich aufgenommen werden und gleich einem Framework behandelt
werden.

React wurde im selben Jahr von dem Facebook-Entwickler Jordan Walke entwickelt, in dem die
Webcomponents-Standards vorgestellt wurden. Als Open-Source-Projekt veröffentlicht wurde React zwei
Jahre darauf im Mai 2013. Obwohl dieser zeitliche Verlauf Parallelen zu dem der Webcomponents
aufweist, wurde React jedoch nie als Alternative dieser beworben. Die Entwicklung von React hat
ihren Ursprung in den komplexen Anforderungen der Anwendungen Facebook und später auch Instagram.
\cite[S. 27]{schmidtWebMobileDeveloperKomponenten}

React wurde nach eigener Aussage entwickelt, um bestehende Projekte sukzessive anpassen zu können
und nur das Maß an React verwenden zu müssen das benötigt wird. Trotzdem ermöglicht die Bibliothek
den Entwicklern komplexe Webanwendung auf Basis von React zu entwickeln.
\cite[]{reactGettingStarted}

Laut eigener Beschreibung ist React eine deklarative, effiziente und flexible JavaScript-Bibliothek,
die es ermöglicht komplexe \acp{ui} zu entwickeln. Hierbei setzt React für das Markup vollständig
auf JavaScript. Es muss lediglich eine \ac{html}-Datei mit einem Container-Element beziehungsweise
Wurzel-Knoten existieren, in welches das generierte Markup eingefügt wird. Für vollständige
React-Projekte als \ac{spa} hat es sich durchgesetzt eine Datei \inlinecode{index.html}
bereitzustellen, die die Grundstruktur der Website enthält sowie als Wurzel-Knoten in der Regel ein
\inlinecode{<div>}-Tag mit der id \inlinecode{root}. \cite[What is React?]{reactTutorial}

\begin{lstlisting}[float, caption=\glqq Hello World\grqq{}-Komponente in React, label=code:helloWorldReact]
// React-Komponente in JavaScript
class HelloWorld extends React.Component {
	render() {
		return React.createElement('h1', null, 'Hello World');
	}
}

// React-Komponente in JSX
class HelloWorld extends React.Component {
	render() {
		return <h1>Hello World</h1>;
	}
}

// React-Komponente als Funktion in JSX
function HelloWorld() {
	return <h1>Hello World</h1>;
}
\end{lstlisting}

Code-Beispiel \ref{code:helloWorldReact} zeigt eine einfache \glqq Hello World\grqq{}-Komponente in
drei verschiedenen Notationen, die jedoch alle zu einem identischen Ergebnis führen. Wie bereits im
vorigen Absatz angesprochen, wird in React auch das Markup in JavaScript, beziehungsweise der
JavaScript-Erweiterung \ac{jsx}, implementiert, sodass eine Komponente in einer einzigen Datei
erstellt werden kann. Hierzu kann reines JavaScript wie im ersten Fall des genannten Beispiels
genutzt werden. Jedes Element kann so über die Funktion \inlinecode{React.createElement(component,
props, ...children)} erstellt werden. Der Parameter \inlinecode{component} ist hierbei das zu
erzeugende Element. Also entweder eine React-Komponente oder, wie in diesem Fall, ein beliebiges
\ac{html}-Element. Mittels \inlinecode{props} können dem Element Properties hinzugefügt werden und
mittels beliebig vieler \inlinecode{children}-Parameter werden die Kind-Elemente des zu erzeugenden
Elements bestimmt. \cite[]{reactNoJsx}

Im zweiten Fall des Code-Beispiels wird die von React geschaffene \ac{jsx}-Syntax verwendet.
\ac{jsx} ist eine Erweiterung der JavaScript-Syntax, die einer \ac{xml}-Struktur nachempfunden ist.
Auf diese Weise kann das Markup in annähernd aus \ac{html} gewohnter Schreibweise definiert werden,
jedoch mit dem Vorteil, dass innerhalb geschweifter Klammern beliebiges JavaScript eingebettet
werden kann. All das unterstützt den React-Ansatz keine Separierung nach Technologien vorzunehmen,
sondern ausschließlich nach Zuständigkeiten, was zu einer hohen Kopplung von Darstellung und
\ac{ui}-Logik führt. \cite[]{reactJsx}

Die letzte gezeigte Variante stellt dar, dass einfache Komponenten ohne eigenen State und ohne die
Nutzung der zahlreichen \glspl{lifecycleCallback} auch als Funk\-tion geschrieben werden können, die
lediglich den Inhalt der render-Funktion einer Klassen-Komponente zurückgeben.

Der bereits im letzten Absatz angesprochene State ist eines von Reacts Kernelementen. Im Gegensatz
zu den Props, die in React unveränderliche Eingabeparameter für Komponenten sind und diesen in der
\ac{jsx}-Notation analog zu Attributen in \ac{html} übergeben werden, spiegelt der State den Zustand
einer Komponente wieder. Dieser Zustand steht nur in Klassen-Komponenten zur Verfügung und muss im
Konstruktor der Komponente initialisiert werden. In Code-Beispiel \ref{code:stateReact} ist eine
solche Initialisierung in Zeile 3 gezeigt. Der State kann gegenüber den Props jederzeit über die
Funktion \inlinecode{this.setState()} aktualisiert werden. Auch dies ist in Code-Beispiel
\ref{code:stateReact}, in den Zeilen 7-9, gezeigt. \cite[]{reactStateLifecycle}
\cite[]{reactComponentsProps}

\begin{lstlisting}[float, caption=State in React, label=code:stateReact]
constructor(props) {
	super(props);
	this.state = {text: "Hello World"};
}
...
doSomething() {
	this.setState({
		text: "Hello Universe"
	});
}
\end{lstlisting}

In den seltensten Fällen hat jede React-Komponente ihren eigenen, isolierten State, da oft mehrere
Komponenten auf eine gemeinsame Datenbasis zugreifen. Betrachtet man beispielsweise ein beliebiges
Eingabeformular mit mindestens zwei Elementen, so ist es wenig sinnvoll, dass jedes Eingabe-Element
seine Werte selbst speichert, da in der Regel die Daten des gesamten Formulars weiterverarbeitet
werden müssen. Aus diesen Gründen empfiehlt React den State in der Komponente anzusiedeln, die als
nächster gemeinsamer Vorfahre der Komponenten dient, die gemeinsame Daten verarbeiten. Die Daten,
die vorher im State der Komponenten verwaltet wurden, werden nun als Props an diese übergeben. Die
Änderung des gemeinsamen State erfolgt über \gls{callback}-Funktionen, die der Komponente ebenfalls
als Props übergeben werden. Dies heißt nicht, dass nicht jede Komponente ihren eigenen State haben
kann. Lediglich die Teile des State, die mehrere Komponenten gemeinsam haben, sollten in einer
gemeinsamen Eltern-Komponente beheimatet sein. \cite[]{reactLiftingStateUp}

Die im Zusammenhang mit Funktions-Komponenten bereits angesprochenen \glspl{lifecycleCallback}
werden zu verschiedenen Zeitpunkten des Komponenten-Lebenszyklus aufgerufen. Hierzu zählen unter
anderem die \inlinecode{constructor()}-Funktion, sowie die Funktion
\inlinecode{componentDidMount()}, die aufgerufen wird, nachdem die Komponente instanziiert und in
das \ac{dom} eingefügt wurde. Wird die Komponente zum Beispiel durch Props verändert, wird
\inlinecode{componentDidUpdate()} aufgerufen. Sofern die Komponente aus dem \ac{dom} entfernt wird,
wird \inlinecode{componentWillUnmount()} ausgeführt. Darüber hinaus zählt auch \inlinecode{render()}
zu den \glspl{lifecycleCallback}, die sowohl bei einer Änderung als auch dem Hinzufügen einer
Komponente aufgerufen wird. \cite[]{reactStateLifecycle} Es existieren noch ein paar weitere
Funktionen, die jedoch verhältnismäßig selten genutzt werden und somit in diesem Kontext irrelevant
sind.

Ein weiteres Hauptmerkmal von React ist, dass es ein eigenes virtuelles \ac{dom} implementiert.
Anders als in \ac{html} handelt es sich bei allen Elementen, nativen \ac{html}-Elementen, wie auch
React-Komponenten nicht um \ac{dom}-Elemente, sondern um unveränderliche JavaScript-Objekte, die in
eben diesem virtuellen \ac{dom} verwaltet werden. Ändern sich die Props oder der State eines
Elements des virtuellen \ac{dom}, so wird dieses Element neu erzeugt und das virtuelle \ac{dom} neu
gerendert. React führt dann einen Vergleich des virtuellen \ac{dom} mit dem realen \ac{dom} durch
und überträgt nur an den Stellen, wo Differenzen festgestellt werden, die Änderungen und macht sie
somit sichtbar. \cite[]{reactRendering}

Zuletzt bleibt noch das Thema Spezialisierung von Komponenten. Da React mit JavaScript-Objekten
arbeitet und die objektorientierte Programmierung weit verbreitet ist, ist die Versuchung groß,
Komponenten über Vererbung zu spezialisieren. Hiervon wird jedoch ausdrücklich abgeraten, da dies
dem komponentenorientierten Ansatz von React widerspricht. Laut eigenen Angaben existiert kein
Problem, welches sich nicht mit Komposition statt Vererbung lösen lässt. So erfolgt die
Spezialisierung von Komponenten in der Regel dadurch, dass die spezielle Komponente, eine
generischere nutzt und diese über Props konfiguriert, sowie möglicherweise weiteren Code hinzufügt.
\cite[]{reactCompositionInheritance}
