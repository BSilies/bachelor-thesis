\subsection{LitElement}
\label{ssec:litElement}
LitElement ist wie React kein Framework, sondern im Wesentlichen eine Klasse einer Bibliothek. Es
nutzt hierbei lit-html, eine schlanke Bibliothek, die es auf einfache Art ermöglicht
\ac{html}-Templates in JavaScript mit integrierten JavaScript-Ausdrücken zu schreiben und diese
effizient zu rendern.

Um LitElement vollständig beschreiben zu können, bedarf es einer grundlegenden Beschreibung von
lit-html, welches auch ohne LitElement genutzt werden kann und in einigen anderen
(Webcomponents-)Frameworks auch genutzt wird. Beschrieben werden soll in diesem Kontext nur der Teil
der Bibliothek, die sich mit dem Schreiben der Templates beschäftigt, da dies für LitElement
relevant ist.

Templates in lit-html werden mittels \glqq Tagged Template Literals\grqq{} geschrieben. Dies sind
Ausdrücke, die von Gravis (engl: Backticks) eingeschlossen werden und denen ein Tag vorangestellt
ist. Ein Tag entspricht einer Funktion, die mit dem Inhalt des Template-Literals ausgeführt wird.
Entgegen gewöhnlicher Strings können Template-Literale über mehrere Zeilen geschrieben werden und
dürfen JavaScript-Ausdrücke enthalten. \cite[]{mdnTemplateLiterals}

Im Falle von lit-html lautet das Tag \inlinecode{html}, dessen Funktion ein
\inlinecode{TemplateResult}-Objekt zurückgibt, welches gerendert werden kann. Code-Beispiel
\ref{code:litHtmlTemplates} stellt vier einfache Beispiele dar, wie ein lit-html-Template aussehen
kann. Zeile 1 zeigt einfaches \ac{html} ohne dynamische Inhalte, während in Zeile 2 der über den
Parameter \inlinecode{title} eingegebene Inhalt dargestellt wird. Zeile 3 und 4 zeigen, dass aber
auch beliebige Ausdrücke in Templates eingebettet werden können. Alle Ausdrücke haben gemeinsam,
dass sie gemäß Spezifikation in geschweifte Klammern eingeschlossen sind, denen ein Dollar-Symbol
vorangestellt ist (\inlinecode{\$\{Ausdruck\}}). In lit-html wird die Einbettung von Ausdrücken als
Binding bezeichnet.

\begin{lstlisting}[float, caption=lit-html Templates, label=code:litHtmlTemplates]
const template1 = html`<div>Hello World</div>`;
const template2 = (title) => html`<h1>${title}</h1>`;
const template3 = (num1, num2) => html`<div>Sum: ${num1 + num2}</div>`;
const template4 = (name) => html`<div>${formatName(name)}</div>`;
\end{lstlisting}

Diese Bindings beschränken sich jedoch nicht auf Inhalte, sondern können auch auf Attribute,
Properties und \glspl{event} angewendet werden, wie in Code-Beispiel \ref{code:litHtmlBindings} zu
sehen ist. Zeile 1 zeigt hierbei ein gewöhnliches Attribut-Binding, bei welchem jedoch darauf
geachtet werden sollte, dass der zugewiesene Wert ein String ist oder zu diesem konvertiert werden
kann. Boolean-Attribute erhalten in lit-html ein vorangestelltes Fragezeichen wie die Zeilen 3 und 4
zeigen. Dies bewirkt, dass das Attribut hinzugefügt wird, wenn der angegeben Ausdruck
\inlinecode{true} ergibt und entfernt wird, wenn \inlinecode{false} zugewiesen wurde.
JavaScript-Properties können direkt angesprochen werden, indem dem Property-Namen ein Punkt
vorangestellt wird, wie Zeile 6 des Beispiels zeigt. Auf diese Weise können einem Element beliebige
Daten übergeben werden, sodass nicht die Restriktion herrscht, dass der Wert zu einem String
konvertiert werden kann. Funktionen können an \glspl{event} gebunden werden, indem der
\gls{event}-Name mit einem @ versehen wird, wie Zeile 7 des Beispiels zeigt.

\begin{lstlisting}[float, caption=lit-html: weitere Bindings, label=code:litHtmlBindings]
const template1 = (data) => html`<p class=${data.style}>...</p>`;

const template2 = (data) => html`
	<option ?disabled=${!data.active}>...</option>`;

const template3 = (data) => html`<input .value=${data.value}></input>`;
const template4 = (fn) => html`<input @change=${fn}></input>`;
\end{lstlisting}

Darüber hinaus besteht die Möglichkeit Templates zu schachteln und so neue Templates zu komponieren.
Dies geschieht durch Integration eines Templates über die vorgestellte Binding-Syntax, wie auch
Code-Beispiel \ref{code:litHtmlKomposition} zeigt.

\begin{lstlisting}[float, caption=lit-html: Komposition, label=code:litHtmlKomposition]
const myHeader = html`<h1>Header</h1>`;
const myPage = html`
	${myHeader}
	<div>Here's my main page.</div>
`;
\end{lstlisting}

Bedingte Templates (engl.: conditional templates) werden in lit-element durch
JavaScript-Verzweigungen erstellt. So besteht die Möglichkeit über den ternären Operator --
vergleiche Code-Beispiel \ref{code:litHtmlConditional} -- ohne die Deklaration einer Funktion
unterschiedliche Templates zu liefern. Genutzt werden kann aber auch die gewöhnliche If-Else-Syntax
in einer Funktion, die je nach Bedingung das entsprechende Template liefert.

\begin{lstlisting}[float, caption=lit-html: Bedingte Templates, label=code:litHtmlConditional]
html`
	${user.isloggedIn
		? html`Welcome ${user.name}`
		: html`Please log in`
	}
`;
\end{lstlisting}

Sich wiederholende Templates können analog zu bedingten Templates geschrieben werden. Dies kann
entweder mittels der \inlinecode{Array.map}-Funktion ohne weitere Funktions-Deklaration oder durch
Aufruf einer Funktion, die mittels einer Schleife sich wiederholende Templates generiert, realisiert
werden. \cite[]{litHtmlTemplates}

Lit-html bietet einige vordefinierte Direktiven, die bei der Gestaltung und auch Performance der
Templates unterstützen. Um das Styling zu erleichtern, existieren zwei Direktiven -
\inlinecode{styleMap} und \inlinecode{classMap}. Letzt genannte weist einem Element \ac{css}-Klassen
anhand eines definierten Objektes zu, welches Bedingungen für Klassen definiert. In Code-Beispiel
\ref{code:litHtmlClassMap} wird zum Beispiel dem \inlinecode{<div>}-Element die Klasse
\inlinecode{selected} zugewiesen, wenn \inlinecode{item.selected} einen Wert enthält, der
\inlinecode{true} entspricht.

\begin{lstlisting}[float, caption=lit-html: classMap, label=code:litHtmlClassMap]
const itemTemplate = (item) => {
	const classes = {selected: item.selected};
	return html`<div class="menu-item ${classMap(classes)}">Text</div>`;
}
\end{lstlisting}

Mittels der \inlinecode{styleMap}-Direktive können dem \inlinecode{style}-Attribut eines Elements die
im zugehörigen Objekt definierten Werte zugewiesen werden. Dieses Vorgehen wird durch Code-Beispiel
\ref{code:litHtmlStyleMap} dargestellt.

\begin{lstlisting}[float, caption=lit-html: styleMap, label=code:litHtmlStyleMap]
const myTemplate = () => {
	styles = {
		color: myTextColor,
		backgroundColor: highlight ? myHighlightColor : myBackgroundColor,
	};
	return html`<div style=${styleMap(styles)}>Text</div>`;
};
\end{lstlisting}

Innerhalb des Templates können selbstverständlich auch \inlinecode{<style>}-Tags genutzt werden, um
den Style mittels \ac{css} zu definieren. Zu beachten ist jedoch, dass innerhalb der
Style-Definition keine Bindings verwendet werden dürfen, da dies zu Problemen führen kann. Grund
hierfür ist, dass diese Konstrukte zum einen durch die Stylesheet-Optimierung der Browser, zum
anderen aber auch durch Polyfills, die für ältere Browser benötigt werden, nicht verarbeitet werden
können. \cite[]{litHtmlStyling}

Alle Direktiven vorzustellen ist in diesem Rahmen nicht nötig und möglich. Jedoch sollen in
Kurzfassung zwei weitere erläutert werden, die die Performance der Anwendung beeinflussen können.
Mit der \inlinecode{repeat}-Direktive existiert eine weitere Möglichkeit sich wiederholende
Templates zu definieren. Im Gegensatz zu den bereits genannten Möglichkeiten, fügt
\inlinecode{repeat} dem Element einen eindeutigen Key hinzu. Dieser ermöglicht es Umstrukturierungen
auf den sich wiederholenden Elementen durch Umsortierung der Elemente vorzunehmen, während die
anderen Möglichkeiten die Werte der Elemente neu zuweisen müssen. Mittels der
\inlinecode{cache}-Direktive kann für das übergebene Template festgelegt werden, dass dessen Inhalt
gecached werden soll. Auf diese Weise können zum Beispiel bei bedingten Templates alle Verzweigungen
gecached werden, sodass das jeweilige Template bei einem Wechsel nicht neu gerendert werden muss,
sondern dieses lediglich aus dem Cache geladen und auf die neusten Daten aktualisiert wird.
\cite[]{litHtmlTemplates}

LitElement ist der nativen Entwicklung von Webcomponents verhältnismäßig ähnlich, weist aber dennoch
einige Unterschiede auf. Statt gemäß der Custom Elements-Spezifikation von \inlinecode{HTMLElement}
oder einer anderen Klasse zu erben, die einem spezifischen \ac{html}-Element entspricht, erben alle
Komponenten von der Basisklasse \inlinecode{LitElement}. Dies sorgt dafür, dass standardmäßig ein
Shadow \ac{dom} erzeugt wird, in dem der Komponenteninhalt gerendert wird. Um Inhalte zu rendern,
muss jede Komponente eine \inlinecode{render}-Funktion definieren, die ein Template, welches
mittels lit-html geschrieben wurde, zurückgibt. Registriert werden muss die Komponente analog zu den
Custom Elements (siehe Kapitel \ref{ssec:customElements}).

Um die optimale Performance aus LitElement zu ziehen, muss beachtet werden, dass ein Template
ausschließlich von den Properties der Komponente abhängt und das Template selbst keine
Zustandsänderungen vornimmt oder sonstige Seiteneffekte hat. Darüber hinaus sollen Änderungen am
\ac{dom} ausschließlich über die \inlinecode{render}-Funktion vorgenommen werden.

Um einer Komponente zu ermöglichen Kind-Elemente zu akzeptieren, können die in Kapitel
\ref{ssec:htmlTemplates} beschriebenen Slots verwendet werden. Diese verhalten sich in LitElement
genauso wie in einem gewöhnlichen \ac{html}-Template. \cite[]{litElementTemplates}

Properties können von LitElement verwaltet werden, wenn diese als solche deklariert werden. Dies
geschieht, indem sie dem Properties-Objekt in dessen Getter hinzugefügt werden, der wie in
Code-Beispiel \ref{code:litElementProperties}, jedoch innerhalb der Klasse, definiert sein muss. Die
dort anzugebenden \inlinecode{options} sind ein Objekt, welches zusätzliches Verhalten und Details
zu der Property definieren kann. Auf diese Parameter wird zu späterem Zeitpunkt weiter eingegangen.

\begin{lstlisting}[float, caption=Property-Deklaration in LitElement, label=code:litElementProperties]
static get properties() {
	return {
		propertyName: options
	};
}
\end{lstlisting}

Die Deklaration von Properties sorgt dafür, dass LitElement eine Reihe von Aufgaben ausführt. Durch
automatische Überwachung der Properties kann eine Aktualisierung der Komponente stattfinden, sobald
eine Wertänderung festgestellt wird. Diese Änderungen werden durch direkten Vergleich des alten und
neuen Wertes registriert. Weiterhin stellt LitElement sicher, dass auch Werte, die einer Property
zugewiesen wurden bevor die Komponente registriert wurde, korrekt übernommen werden. Auch wird zu
jeder registrierten Property die Überwachung eines gleichnamigen Attributs sowie die Konvertierung
zwischen dem String-Wert des Attributs und dem Wert des Typs der Property eingerichtet. Guter Stil
ist es den deklarierten Properties im Konstruktor einen Initialwert zuzuweisen.

Einige dieser Eigenschaften können über die bereits angesprochenen Optionen geändert werden. So kann
über die Option \inlinecode{attribute} für das zugehörige Attribut ein anderer Name als der der
Property festgelegt werden. Oder aber durch Zuweisung des Wertes \inlinecode{false} wird festgelegt,
dass kein zugehöriges Attribut existiert. Darüber hinaus können Datentyp des Attributs, eigene
Konvertierungsfunktionen für den Transfer zwischen Attribut und Property sowie selbst definierte
Vergleichslogik zum Feststellen einer Property-Änderung festgelegt werden. Ebenso existiert die
Möglichkeit den Wert einer Property auf das zugehörige Attribut zurückzuspiegeln.

Zu jeder Property besteht die Möglichkeit eigene Getter und Setter zu schreiben, wie es in
Code-Beispiel \ref{code:litElementAccessors} gezeigt wird. Hierbei ist jedoch zu beachten, dass im
Setter explizit \inlinecode{requestUpdate} aufgerufen werden muss, um den Wert der Property zu
setzen. Übergeben wird der Funktion, entgegen der Annahme, der alte Wert.
\cite[]{litElementProperties}

\begin{lstlisting}[float, caption=Getter und Setter in LitElement, label=code:litElementAccessors]
set prop(val) {
	let oldVal = this._prop;
	...
	this.requestUpdate('prop', oldVal);
}

get prop() { return this._prop; }
\end{lstlisting}

Das Styling in LitElement erfolgt ähnlich wie das Schreiben der Templates. Um optimale Performance
zu erreichen, empfiehlt LitElement den Style in einer statischen Style-Property zu definieren.
Hierzu wird in der Regel gemäß Code-Beispiel \ref{code:litElementStyling} ein Getter für diese
Property definiert, die das \ac{css}-Template zurückgibt. Das \ac{css}-Template ist analog zu den
\ac{html}-Templates ein \glqq Tagged Template Literal\grqq{}, jedoch mit dem Unterschied, dass das
Tag die Bibliotheks-Funktion \inlinecode{css} ist.

\begin{lstlisting}[float, caption=LitElement Styling, label=code:litElementStyling]
static get styles() {
	return css`
		div { color: red; }
	`;
}
\end{lstlisting}

Um der Ausführung von bösartigem Code entgegenzuwirken, ist in \ac{css}-Templates nur das Binding
anderer Strings oder Zahlen zulässig, die ebenfalls mit \inlinecode{css} getagged sind. Soll dennoch
ein anderer Wert in den Style eingepflegt werden, so muss dieser durch die Funktion
\inlinecode{unsafeCSS} eingeschlossen werden.

Da der Getter neben eines einzelnen Templates auch ein Array von Templates zurückgeben kann, besteht
darüber hinaus die Möglichkeit weitere Styles aus einer separaten JavaScript-Datei zu laden. Dies
kann zum Beispiel für komponentenübergreifende Styles genutzt werden, die so an einer zentralen
Stelle definiert werden können. Der Getter kann so möglicherweise wie in Code-Beispiel
\ref{code:litElementExternalStyles} aussehen, wobei die externe Style-Definition
\inlinecode{externalStyles} importiert wurde.

\begin{lstlisting}[float, caption=LitElement: Externe Styles, label=code:litElementExternalStyles]
import { externalStyles } from './external-styles.js';
static get styles() {
	return [
		externalStyles,
		css`
			...
		`
	]
}
\end{lstlisting}

Darüber hinaus besteht die Möglichkeit \inlinecode{<style>}-Tags im \ac{html}-Template zu verwenden,
sowie externe \ac{css}-Stylesheets per \inlinecode{<link>} zu importieren. Erst genannte Lösung kann
interessant sein, falls Styles benötigt werden, die ausschließlich pro Instanz gelten, da eine
statische Style-Property nur einmal pro Klasse ausgewertet wird. Hier sind jedoch die gleichen
Einschränkungen, wie bereits bei lit-html beschrieben, zu beachten. Letztgenannte Lösung sollte
nicht verwendet werden, da dies zum einen von den verwendeten Polyfills nicht unterstützt wird, aber
zum andern auch der relative Link zur externen Datei zu Problemen führen kann. Darüber hinaus können
Verzögerungen beim Laden der Datei dazu führen, dass der Komponenteninhalt dem Nutzer kurzzeitig
ungestylt präsentiert wird. \cite[]{litElementStyles}

Die \glspl{lifecycleCallback} der Custom Elements-Spezifikation werden in LitElement durch weitere
Funktionen ergänzt, die sich insbesondere um die Aktualisierung der Komponente drehen. So können
beispielsweise mittels des \inlinecode{updated}-\gls{callback}s von Properties abgeleitete Daten auf
dem aktuellsten Stand gehalten werden. Aber auch die bereits erwähnte Funktion
\inlinecode{requestUpdate} gehört zu den \glspl{lifecycleCallback}. Wird diese ohne Parameter
aufgerufen, löst sie eine Aktualisierung des \ac{dom}s aus, was insbesondere dann notwendig ist,
wenn sich der Inhalte eines Objektes oder Arrays ändert, die Referenz auf dieses jedoch
nicht\footnote{Vorausgesetzt es wurde für die entsprechende Property keine benutzerdefinierte Logik
zur Erkennung von Änderungen definiert.}. \cite[]{litElementLifecycle}

LitElement kann mit einem Compiler wie TypeScript oder Babel verwendet werden, was zu einer
Erweiterung der Syntax führt. Wird ein entsprechender Compiler verwendet, können in LitElement zum
Beispiel \glspl{decorator} genutzt werden, um das Custom Element der Komponente implizit zu
definieren oder die Deklaration von Properties zu vereinfachen.
\cite[]{litElementDecorators}
