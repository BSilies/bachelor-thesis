\section{Kapselung von Framework-Komponenten zu Webcomponents}
Bevor auf dieses Thema weiter eingegangen werden kann, soll zuerst klar gestellt werden, was unter
diesem Punkt zu verstehen ist. Unter der Kapselung von Framework-Komponenten zu Webcomponents soll
verstanden werden, dass bestehende Framework-Komponenten in den Rahmen einer Webcomponent
eingebettet werden und somit als solche verfügbar werden. Betrachtet werden sollen in diesem Kontext
auch wieder die Frameworks React, Vue und Angular, da diese zum einen bereits in dieser Arbeit
behandelt wurden, diese zum anderen aber auch die meist genutzten Vertreter der
JavaScript-Frameworks auf Komponentenbasis sind.

Nachvollziehbar ist der Wunsch dieser Möglichkeit aus der Sicht, dass vielfach, wie auch im \ac{lsg}
der \ac{coba}, Bibliotheken mit diversen Komponenten existieren, die allerdings oft nur für ein
bestimmtes Framework entwickelt wurden. Um diese auch für Projekte verfügbar zu machen, die dieses
Framework nicht einsetzen können oder wollen, liegt es nahe, die bestehenden Komponenten innerhalb
von Webcomponents zu kapseln und somit nahezu universell einsetzbar zu machen. Dies spart vor allem
eine aufwändige Neuentwicklung aller Komponenten.

In Kapitel \ref{sec:vergleichWebcomponentsFrameworkKomponenten} wurde bereits festgestellt, dass Vue
und React tendenziell eine gute Basis zur Verwendung in Webcomponents bieten, da Komponenten dieser
Frameworks lediglich ein Host-Element benötigen, um in eine Anwendung eingebunden zu werden. Um eine
vollständige Kompatibilität zu gewährleisten, ist jedoch ein höherer Aufwand notwendig, da beide
Frameworks ein virtuelles \ac{dom} implementieren, das nicht ohne weiteres mit dem Shadow \ac{dom}
einer Webcomponent in Einklang steht.

Am Beispiel React sollen die Schritte erklärt werden, die notwendig sind, um eine React-Komponente
als Webcomponent verfügbar zu machen.

Der erste logische Schritt ist die Erstellung einer Webcomponent, in die die React-Komponente
eingebettet werden soll. Statt des Template-Inhalts wird im zweiten Schritt ein Host-Element für die
React-Komponente, in der Regel ein \inlinecode{<div>}-Element, an das Shadow \ac{dom} der
Webcomponent angehängt. Im \inlinecode{connectedCallback} der Webcomponent kann dann mittels der
React-Funktion \inlinecode{ReactDOM.render()} die React-Komponente an das Host-Element angehängt
werden, wobei diese im \inlinecode{disconnectedCallback} auch wieder von diesem gelöst werden
sollte.

Um auf Property- und Attribut-Änderungen reagieren zu können, sind im dritten Schritt zuerst
dieselben Arbeiten notwendig wie auch in einer gewöhnlichen Webcomponent. Mittels
\inlinecode{observedAttributes} und dem \inlinecode{attributeChangedCallback} sowie Gettern und
Settern wird die entsprechende Basis geschaffen. In dem als Grundlage für diese Beschreibung
genutzten Blog-Post werden die Werte der Attribute und Properties in Objekten gespeichert und diese
destrukturiert als Props an die React-Komponente übergeben. Um auch tatsächlich eine Aktualisierung
zu bewirken, muss nach jeder Änderung an Attributen oder Properties, die an die React-Komponente
übertragen werden, erneut die Funktion \inlinecode{ReactDOM.render()} ausgeführt werden.

Schritt vier besteht darin, auch die Styles der React-Komponente in der Webcomponent anzuwenden.
Prinzipiell muss hierfür ein \inlinecode{<style>}-Tag mit den entsprechenden Styles für die
Komponente in das Shadow \ac{dom} eingefügt werden. Ein im Artikel empfohlenes JavaScript-Paket kann
hierbei unterstützen, notwendig ist dies jedoch nicht.

Als letzten Punkt gilt es das \gls{event}-Handling der Komponente zu bereinigen. \glspl{event}, die
in einem Shadow \ac{dom} geschehen und aus diesem austreten, werden durch die Kapselung des Shadow
\ac{dom} als \glspl{event} des definierten Custom Elements ausgegeben. Dieses Verhalten führt zu
Konflikten mit React, sodass die React-Komponente nicht mehr korrekt auf \glspl{event} reagiert. Um
dieses Problem zu beheben, bedarf es einer expliziten Umleitung der \glspl{event} zurück in den
Kontext, in dem React diese verarbeiten kann. Hierzu empfiehlt es sich ein npm-Paket namens \glqq
react-shadow-dom-retarget-events\grqq{} zu nutzen, das diese Aufgabe für sämtliche \glspl{event}
übernimmt. Hierzu muss nach Einbinden des Pakets in die Webcomponent lediglich die Funktion
\inlinecode{retargetEvents(this)} aufgerufen werden. \cite[]{itnextWebcomponentsReact}

Ob diese Anleitung für jede Komponente anwendbar ist, kann an diesem Punkt nicht gesagt werden, da
diese nicht selbst getestet wurde und dies auch gar nicht für alle erdenklichen Komponenten möglich
ist. Vielmehr dient diese Anleitung als Grundlage, die auf eigene Bedürfnisse angepasst werden kann
und muss.

Die Entwicklung von Webcomponents mittels Vue liefe wahrscheinlich nach einem ähnlichem Prinzip ab,
würde Vue nicht in dem Vue \ac{cli} direkt eine Möglichkeit bieten die Anwendung direkt in
Webcomponents zu konvertieren. Der einzige Unterschied zur Verwendung selbst definierter
Webcomponents besteht darin, dass Vue auch in der Anwendung, die die Komponenten nutzt, eingebunden
werden muss. Die so erzeugten Webcomponents sind jedoch ausdrücklich nicht mit älteren Browsern wie
dem Internet Explorer 11 kompatibel, sodass hier wahrscheinlich auf Polyfills zurückgegriffen werden
muss. \cite[]{vueCliWebcomponents}

Angular bietet mit \glqq Angular Elements\grqq{} einen ähnlichen Ansatz. Die Entwicklung der
Komponenten in Angular erfolgt exakt genauso, wie es ohne das Ziel Webcomponents der Fall ist;
lediglich auf Modulebene müssen geringe Änderungen vorgenommen werden. Das Modul muss, damit
Webcomponents erzeugt werden können, das Interface \inlinecode{DoBootstrap} mit der Methode
\inlinecode{ngDoBootstrap()} implementieren und im Konstruktor die Funktion
\inlinecode{createCustomElement()} aus Angular Elements aufrufen sowie mit der aus dem Standard
bekannten Funktion \inlinecode{customElements.define()} das Custom Element registrieren. Wird die
Anwendung nun dem \gls{build}-Prozess zugeführt, werden fünf JavaScript-Dateien erzeugt, mittels
derer die erzeugten Webcomponents in anderen Anwendungen genutzt werden können. Da sich unter den
Dateien auch Polyfills befinden, können diese in diesem Zustand mit Einschränkungen auch im Internet
Explorer eingesetzt werden. Problematisch sind dynamische Komponenten, da der Internet Explorer die
Änderungserkennung in den Angular-Webcomponents nicht unterstützt. Um eine vollständige
Unterstützung zu gewährleisten, bedarf es einer eigenen sogenannten \glqq Change Detection Zone
Strategy\grqq{}. Diese kann jedoch auch durch ein zusätzliches Paket importiert werden und es werden
nochmals geringe Änderungen im Konstruktor des Moduls nötig. \cite[]{inDepthAngularWebcomponents}

Zusammenfassend lässt sich sagen, dass React-Komponenten zwar ohne zusätzliche \gls{build}-Maßnahmen
zu Webcomponents gekapselt werden können, dies jedoch im Vergleich zu den anderen vorgestellten
Maßnahmen verhältnismäßig aufwändig ist. Vue und Angular gehen ebenso wie eine stetig wachsende Zahl
weiterer Frameworks den Weg, dass die implementierten Komponenten zusätzlich als Webcomponents
ausgegeben werden können, wodurch kein bis minimaler Mehraufwand notwendig ist.
