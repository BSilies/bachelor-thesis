\subsection{Hybrids}
\label{ssec:hybrids}
Die Bibliothek hybrids ermöglicht es Webcomponents allein auf der Basis von JavaScript-Objekten und
Funktionen zu erstellen. Dies hat zur Folge, dass weder Klassen noch die \gls{thisSyntax} benötigt
wird, welches der Schöpfer der Bibliothek -- Dominik Lubański -- als einen der Hauptvorteile seiner
Bibliothek beschreibt. \cite[]{hybridsGettingStarted}

Die Definition einer Komponente erfolgt in hybrids ähnlich zum Custom Elements-Standard. Statt einer
Klasse nutzt hybrids ein JavaScript-Objekt und statt der \ac{dom}-\ac{api}-Funktion
\inlinecode{customElements.define()} wird die Funktion \inlinecode{define()} aus der Bibliothek
genutzt. Code-Beispiel \ref{code:hybridsDefinition} zeigt dies, ebenso wie den strukturellen Aufbau
einer Komponente in Objekt-Notation. Alle Properties, einschließlich dem zu rendernden Template,
werden als Elemente des Objektes notiert.

\begin{lstlisting}[float, caption=Komponentendefinition in hybrids, label=code:hybridsDefinition]
import { define } from 'hybrids';

const MyElement = {
	count: 0,
	render: ({ count }) => {...},
};

define('my-element', MyElement);
\end{lstlisting}

Diese Properties sind in hybrids grundsätzlich unabhängig. Benötigt eine Property den Wert einer
anderen Property, so wird diese aufgerufen und der Wert berechnet. Über einen Mechanismus zur
Änderungserkennung wird eine Aktualisierung des \ac{dom} angestoßen, sofern sich Properties der
Komponente geändert haben. Dies resultiert in einer Struktur, in der das Template ausschließlich vom
Zustand der Komponente, also der Gesamtheit der Properties, abhängig ist. Seiteneffekte, also
Funktionalitäten, die den Zustand der Komponente verändern, können durch Funktionen realisiert
werden, die außerhalb des Kontextes der Komponente liegen. Diese können aus der Komponente heraus
angestoßen werden und über eine mitgegebene Instanz den Zustand dieser verändern, was zu einer
Aktualisierung der Ansicht führen kann. \cite[]{hybridsIntroduction}

Die Definition der Properties erfolgt in hybrids mittels eines sogenannten Deskriptors. Dieser kann
aus bis zu vier Methoden bestehen, die zum Teil ähnliche Zwecke erfüllen, wie es die
\glspl{lifecycleCallback} des Custom Elements-Standards tun. Allen Methoden ist gemein, dass sie als
ersten Parameter eine Instanz eines Elements erwarten, wodurch auch die hier definierten Funktionen
ohne \gls{thisSyntax} auskommen. Einen Überblick über die Syntax eines Deskriptors liefert
Code-Beispiel \ref{code:hybridsDescriptor}.

\begin{lstlisting}[float, caption=Deskriptor in hybrids, label=code:hybridsDescriptor]
const MyElement = {
	propertyName: {
		get: (host, lastValue) => { ... },
		set: (host, value, lastValue) => { ... },
		connect: (host, key, invalidate) => {
			...
			// disconnect
			return () => { ... };
		},
		observe: (host, value, lastValue) => { ... },
	},
};
\end{lstlisting}

Die Methoden \inlinecode{get} und \inlinecode{set} ermöglichen dem Entwickler die Definition eigener
Zugriffsfunktionen. Mittels \inlinecode{get} können zum einen Read-Only-Properties erstellt, zum
anderen aber auch abhängige Properties realisiert werden. Wird diese Methode nicht gesetzt, greift
die Standard-Funktionalität, die den letzten gespeicherten Wert zurückgibt. Um Properties zu
erstellen, die ausschließlich Lesezugriff haben, darf bei gesetzter \inlinecode{get}-Methode, die
\inlinecode{set}-Methode nicht definiert sein. Sind beide Methoden nicht gesetzt, so greift auch für
\inlinecode{set} eine Standard-Funktion, die den übergebenen Wert speichert. Soll \inlinecode{set}
selbst definiert werden, so ist zu beachten, dass der neu berechnete und zu speichernde Wert
ebenfalls zurückgegeben werden muss. Dies ist unter anderem notwendig, um den im Framework
implementierten Cache mit dem neuen Wert zu versehen. Dieser Cache wirkt insbesondere bei lesenden
Zugriffen auf eine Property, da nur der erste Zugriff den Wert berechnet, dieser bei folgenden
Zugriffen jedoch aus dem Cache gelesen wird. Ein erneuter Aufruf des Getters erfolgt nur, sofern
sich der Wert der Property durch Setzen eines neuen Wertes oder durch Änderung von Abhängigkeiten
verändert oder dieser manuell als ungültig gekennzeichnet wird.

Die Methode \inlinecode{connect} erfüllt die Aufgabe der \glspl{lifecycleCallback}
\inlinecode{connectedCallback} und \inlinecode{disconnectedCallback}. Die im Funktionsrumpf
definierten Anweisungen werden bei Verbindung der Komponente mit dem \ac{dom} ausgeführt. Optional
kann eine Funktion zurückgegeben werden, die bei Trennung der Verbindung ausgeführt wird. Mittels
des Parameters \inlinecode{invalidate} -- einer \gls{callback}-Funktion -- besteht die im vorigen
Abschnitt beschriebene Möglichkeit den Wert der Property für Ungültig zu erklären und somit eine
Neuberechnung zu erzwingen. Dies ist laut Dokumentation insbesondere für die Anbindung asynchroner
\acp{api} oder externer Bibliotheken nützlich und notwendig.

Die \inlinecode{observe}-Methode ermöglicht Seiteneffekte, die durch die Änderung der Property
ausgelöst werden sollen. Vergleichbar ist dies mit dem \inlinecode{attributeChangedCallback}, jedoch
mit dem großen Unterschied, dass sich \inlinecode{observe} ausschließlich auf die Property, nicht
aber auf das Attribut bezieht. Die hier definierte Funktionalität wird standardmäßig asynchron
ausgeführt.

An dieser Stelle sei noch einmal hervorgehoben, dass die Methoden des Deskriptors pro Property und
nicht pro Komponente definiert werden. Dies ist insbesondere für die beiden zuletzt vorgestellten
Methoden ein Gegensatz zu den verwandten \glspl{lifecycleCallback} des Custom Elements-Standard.
\cite[]{hybridsDescriptors}

Ein weiteres Kern-Konzept von hybrids sind Factories, die genutzt werden können, um die zuvor
beschriebenen Property-Deskriptoren zu erzeugen. Anwendung finden diese insbesondere dort, wo
mehrere Properties ähnliche Deskriptoren hätten und so eine Code-Redundanz vermieden werden kann.
Eine Vorgabe über Parameter existiert für Factories nicht, sodass durch den Entwickler beliebige
Parameter zur Anpassung des Deskriptors übergeben werden können. Als Rückgabewert wird in jedem Fall
ein Deskriptor-Objekt im zuvor beschriebenen Format erwartet. Ein Beispiel auch zur Nutzung einer
Factory zeigt das Code-Beispiel \ref{code:hybridsFactory}. \cite[]{hybridsFactory}

\begin{lstlisting}[float, caption=Factories in hybrids, label=code:hybridsFactory]
function factory(defaultValue) {
	return {
		...
	};
}

const MyElement = {
	value: factory('text'),
};
\end{lstlisting}

Vier vordefinierte Factories bietet hybrids bereits. Allen voran steht die
\inlinecode{property}-Factory, die genutzt wird um Properties zu erzeugen. Sie erwartet einen
Standardwert, sowie optional eine \inlinecode{connect}-Funktion (siehe Absatz zu Deskriptoren).
Zurück gibt sie einen Deskriptor, der die Methoden \inlinecode{get}, \inlinecode{set} und
\inlinecode{connect} beinhaltet. \cite[]{hybridsProperty}

Die Factories \inlinecode{parent} und \inlinecode{children} dienen in hybrids dazu eine Verbindung
zwischen einem Element und einem übergeordneten Knoten oder seinen Kind-Knoten herzustellen wie es
Code-Beispiel \ref{code:hybridsParent} zeigt. Dies ermöglicht es anderen Properties in Abhängigkeit
zu Eltern- oder Kind-Knoten zu stehen oder aber auch Seiteneffekte durch Änderung von Eltern- oder
Kind-Knoten auszulösen. Möglich ist dies jedoch nur mit Elementen, die ebenfalls in hybrids erstellt
wurden. \cite[]{hybridsParentChildren}

\begin{lstlisting}[float, caption=hybrids Parent-Factory, label=code:hybridsParent]
import { parent } from 'hybrids';

const ParentComponent = {
	count: 0,
};

const ChildComponent = {
	store: parent(ParentComponent),
	label: ({ store: { count } }) => `parent count: ${count}`,
}
\end{lstlisting}

Die \inlinecode{render}-Factory hat den Zweck die \ac{dom}-Struktur der Komponente zu erstellen und
auf dem aktuellen Stand zu halten. Die Factory erwartet als Parameter eine \gls{callback}-Funktion
mit einem \inlinecode{host}-Parameter sowie ein Options-Objekt mittels dessen gesteuert werden
kann, ob ein Shadow \ac{dom} angelegt werden soll, und wenn ja, mit welchen Eigenschaften. Das
Template wird innerhalb der \gls{callback}-Funktion definiert und kann grundsätzlich mit einer
beliebigen Template-Engine\footnote{Zum Beispiel lit-html.} geschrieben werden. Trotzdem liefert
hybrids auch eine eigene Engine in der Bibliothek mit. \cite[]{hybridsRender}

Das Framework bietet einen eigenen Übersetzungsmechanismus, der es erlaubt bestimmte
Kurzschreibweisen zu verwenden, die durch diesen Mechanismus entweder in die beschriebenen
eingebauten Factories umgewandelt werden oder die verkürzte Syntax ausweitet. Für den
Übersetzungsmechanismus werden drei Regeln angewendet. Die erste Regel wandelt eine Property mit dem
Schlüssel \inlinecode{render} und einer Funktion als Wert in eine \inlinecode{render}-Factory um.
Liegt das gleiche mit einem anderen Schlüssel vor, so wird durch die zweite Regel ein Deskriptor für
diese Property ausschließlich mit einem Getter erstellt. Regel Nummer drei wandelt den primitiven
oder Array-Wert eines Schlüssels in eine \inlinecode{property}-Factory mit dem Wert als
Standardwert um. \cite[]{hybridsTranslation}

Die Template-Engine von hybrids weist starke Ähnlichkeiten zu dem bereits vorgestellten lit-html
auf, da sich der Hauptentwickler durch dieses hat inspirieren lassen. So werden ebenso wie in
lit-html Template-Literale verwendet, die mit der Funktion \inlinecode{html} getagged sind. Als
zusätzliches Tag bietet hybrids \inlinecode{svg} für \ac{svg}-Templates.
\cite[]{hybridsTemplateOverview}

Da lit-html bereits bekannt ist, sollen im Folgenden nur die wesentlichen Unterschiede und
Feinheiten herausgearbeitet werden. Auf eine Nennung aller Unterseiten der Web-Quelle soll Zwecks
Übersichtlichkeit verzichtet werden. Entnommen sind sämtliche Informationen der Internetseite
\cite[]{hybridsGettingStarted} unter dem Unterpunkt \glqq Template Engine\grqq{}.

In hybrids wird nahezu ausschließlich mit Properties gearbeitet und Attribute werden nur ein
einziges Mal beim initialen Laden der Komponente ausgewertet. Aus diesem Grund wird keine manuelle
Unterscheidung wie in lit-html bei der Übergabe von Werten vorgenommen. Werden statische
String-Werte übergeben, so wird das Attribut angesprochen; in allen anderen Fällen die Property. Das
\inlinecode{class}- und \inlinecode{style}-Attribut haben eine Sonderstellung in hybrids. Die
Klassendefinition kann sowohl durch einen String als auch durch ein Array aus Klassennamen und einem
Objekt analog einer \inlinecode{classMap} in lit-html erfolgen. Das \inlinecode{style}-Attribut kann
ebenfalls durch ein Objekt analog der \inlinecode{styleMap} definiert werden. \glspl{eventListener}
werden in hybrids auf dieselbe Weise definiert wie sie es ohne Framework werden. Für bedingte
Templates werden dieselben Mechanismen wie in lit-html benutzt. Wiederholungen werden ebenfalls und
ausschließlich mittels \inlinecode{Array.map} herbeigeführt. Die Verschachtelung von Templates wurde
ebenso ermöglicht. Das Styling in hybrids hingegen funktioniert nach denselben Maßstäben wie im
\ac{html} Templates-Standard (siehe \ref{ssec:htmlTemplates}) beschrieben, sodass der Style mittels
\inlinecode{<style>}-Tags im Template definiert wird. Zu beachten sind hierbei jedoch die selben
Einschränkungen, wie auch bei lit-html, sofern diese Methode dort eingesetzt wird.
