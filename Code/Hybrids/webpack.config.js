const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = (env, argv)=>({
	mode: 'development',
	entry: {
		app: './src/index.js'
	},
	devtool: argv.mode === 'production'? 'none' : 'inline-source-map',
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
        historyApiFallback: true,
		hot: true
	},
	module: {
		rules: [
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: [
					'file-loader',
				],
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
				},
			},
		],
	},
	resolve: {
        extensions: ['.tsx', '.ts', '.js']
	},
	output: {
		filename: '[name].bundle.js',
		path: path.resolve(__dirname, 'dist'),
	},
	plugins: [
		new CleanWebpackPlugin(),
		new HtmlWebpackPlugin({
			title: 'LSG Accordion',
			template: './src/template.html'
		}),
		new CopyWebpackPlugin({
			patterns: [
				{
					context: 'node_modules/@webcomponents/webcomponentsjs',
					from: '**/*.js',
					to: 'webcomponents'
				}
			]
		}),
	],
});
