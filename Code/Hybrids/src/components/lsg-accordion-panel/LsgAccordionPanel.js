import { define, html } from "hybrids";

export const LsgAccordionPanel = {
	render: () => html`
		<slot></slot>
	`
};

define('lsg-accordion-panel', LsgAccordionPanel);
