import { define, html, children } from "hybrids";
import { LsgAccordionElement } from "../lsg-accordion-element/LsgAccordionElement";
import { Mode } from "../../../../Common/LsgAccordionSettings";
export * from "../lsg-accordion-element/LsgAccordionElement.js";

export const LsgAccordion = {
	mode: {
		connect: (host, key, invalidate) => {
			host[key] = host.hasAttribute('mode') ? host.getAttribute('mode') : Mode.MULTIPLE;
			registerChildren(host);
			host.addEventListener('toggle', (e) => {handleToggle(host, e)});
			host.observer = new MutationObserver(() => registerChildren(host));
			host.observer.observe(host, {childList: true})
			return () => {
				host.removeEventListener('toggle', (e) => {handleToggle(host, e)});
				host.observer.disconnect();
			}
		},
		observe: (host, val, lastVal) => {
			console.timeStamp("setMode");
			checkMode(host);
		}
	},
	render: () => html`
		<style>
			::slotted(lsg-accordion-element:last-child){
				--display-hr: none;
			}

			:host{
				margin: 32px 0;
				display: block;
				--color-ending-line: #eaeaea;
				--color-headline-background: #ffffff;
				--color-headline-background-hover: #f4f4f4;
				--color-headline-text: #000000;
				--color-icon: #000000;
			}
		</style>
		<slot></slot>
	`
};

function registerChildren(host) {
	host._children = [];
	for (let i = 0; i < host.children.length; i++) {
		host._children.push(host.children.item(i));
	}
	checkMode(host);
}

function checkMode(host) {
	if (host.mode != Mode.MULTIPLE && host._children.length > 0) {
		let openChildren = host._children.filter(child => child.isOpen);
		if (openChildren.length > 1) {
			openChildren.shift();
			openChildren.forEach(child => child.isOpen = false);
		} else if (openChildren.length == 0 && host.mode == Mode.STRICT) {
			host._children[0].isOpen = true;
		}
	}
	console.timeStamp("checkModeEnd");
}

function handleToggle(host, e) {
	switch (host.mode) {
		case Mode.STRICT:
			if (!e.srcElement.isOpen) {
				try {
					let openChild = host._children.find(child => child.isOpen);
					openChild.isOpen = false;
				} catch { ; }
				e.srcElement.isOpen = true;
			}
			break;
		case Mode.SINGLE:
			if (!e.srcElement.isOpen) {
				try {
					let openChild = host._children.find(child => child.isOpen);
					openChild.isOpen = false;
				} catch { ; }
				e.srcElement.isOpen = true;
			} else {
				e.srcElement.isOpen = false;
			}
			break;
		default:
			e.srcElement.isOpen = !e.srcElement.isOpen;
			break;
	}
	console.timeStamp("handleToggleEnd");
}

define('lsg-accordion', LsgAccordion);
