import { define, html, dispatch } from "hybrids";
export * from "../lsg-accordion-header/LsgAccordionHeader.js"
export * from "../lsg-accordion-panel/LsgAccordionPanel.js"

export const LsgAccordionElement = {
	headertext: '',
	isOpen: false,
	render: ({headertext, isOpen}) => html`
		<style>
			hr {
				height: 1px;
				border: none;
				background-color: var(--color-ending-line, #eaeaea);
				margin: 0 16px;
				display: var(--display-hr, block);
			}

			hr.open {
				margin-top: 16px;
			}

			lsg-accordion-panel{
				display: block;
				padding-left: 32px;
				padding-right: 16px;
				height: auto;
				transition: height 2s ease-out;
			}

			lsg-accordion-panel.collapsed {
				display: none;
				overflow: hidden;
				height: 0;
			}

			::slotted(*){
				margin-left: 0;
				margin-right: 0;
				font-family: verdana, sans-serif;
			}
		</style>
		<lsg-accordion-header
			headertext="${headertext}"
			isOpen="${isOpen}"
			onclick="${handleClick}">
		</lsg-accordion-header>
		<lsg-accordion-panel
			class="${{collapsed: !isOpen}}"
			id="panel">
			<slot></slot>
		</lsg-accordion-panel>
		<hr class="${{open: isOpen}}">
	`
};

function handleClick(host){
	console.timeStamp("click");
	dispatch(host, 'toggle', { detail: {isOpen: host.isOpen}, bubbles: true, composed: true });
}

define('lsg-accordion-element', LsgAccordionElement);
