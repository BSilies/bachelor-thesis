import { define, html } from "hybrids";

export const LsgBasicHeadline = {
	content: '',
	render: ({content}) => html`
		<style>
			h3 {
				font-family: "Arial Bold", sans-serif;
				float: inherit;
				margin: 0;
			}
		</style>
		<h3>${content}</h3>
	`
}

define('lsg-basic-headline', LsgBasicHeadline);
