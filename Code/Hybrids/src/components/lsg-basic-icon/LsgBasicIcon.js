import { define, html, property } from "hybrids";
import { Icons, Size } from "../../../../Common/LsgBasicIconSettings";

export const LsgBasicIcon = {
	iconname: iconParamFactory(Icons.CHEVRON_DOWN),
	size: iconParamFactory(Size.SMALL),
	isSolid: iconParamFactory(false),
	icon: undefined,
	render: ({icon}) => html`
		<style>
			img {
				display: block;
			}
		</style>
		<img alt="arrow down" src=${icon}>
	`
};

function iconParamFactory(defaultValue) {
	return {
		...property(defaultValue),
		observe(host) {loadIcon(host)}
	};
}

function loadIcon(host){
	let appearance;
	if(host.isSolid) {
		appearance = 'solid';
	} else {
		appearance = 'outline';
	}
	import(`../../../../Common/Icons/${host.iconname}_${host.size}_${appearance}.svg`)
		.then(loadedIcon => {
			host.icon = loadedIcon.default;
		});
}

define('lsg-basic-icon', LsgBasicIcon);
