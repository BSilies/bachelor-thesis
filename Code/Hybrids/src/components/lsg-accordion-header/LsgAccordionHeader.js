import { define, html } from "hybrids";
import { Icons, Size } from "../../../../Common/LsgBasicIconSettings";
export * from "../lsg-basic-headline/LsgBasicHeadline.js"
export * from "../lsg-basic-icon/LsgBasicIcon.js"


export const LsgAccordionHeader = {
	headertext: '',
	isOpen: false,
	render: ({headertext, isOpen}) => html`
		<style>
			lsg-basic-headline {
				float: left;
				display: block;
				margin: 28px 0;
			}

			lsg-basic-icon {
				position: absolute;
				float: left;
				top: 50%;
				right: 0;
				margin-top: -12px;
				margin-right:32px;
				margin-left:16px;
				transition: transform 0.2s;
			}

			lsg-basic-icon.open {
				-ms-transform: rotate(180deg);
				transform: rotate(180deg);
			}

			.lsg-accordion-header-container {
				overflow: auto;
				position: relative;
				cursor: default;
				padding-left: 16px;
				padding-right: 72px;
				transition: padding 0.2s;
				background-color: var(--color-headline-background, #ffffff)
			}

			.lsg-accordion-header-container:hover {
				background-color: var(--color-headline-background-hover, #f4f4f4)
			}

			.lsg-accordion-header-container:hover, .lsg-accordion-header-container.open {
				padding-left: 32px;
			}
		</style>
		<div class="${{'lsg-accordion-header-container': true, open: isOpen}}">
			<lsg-basic-headline
				content="${headertext}">
			</lsg-basic-headline>
			<lsg-basic-icon
				class=" ${{open: isOpen}}"
				iconname="${Icons.CHEVRON_DOWN}"
				size="${Size.SMALL}">
			</lsg-basic-icon>
		</div>
	`
};

define('lsg-accordion-header', LsgAccordionHeader);
