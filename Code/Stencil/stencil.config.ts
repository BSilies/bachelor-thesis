import { Config } from '@stencil/core';

export const config: Config = {
  namespace: 'stencil',
  taskQueue: 'async',
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader',
    },
    {
      type: 'dist-custom-elements-bundle',
      dir: 'dist-custom-elements-bundle'
    }
  ]
};
