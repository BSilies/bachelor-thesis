import { Component, ComponentInterface, h, Prop, Event, EventEmitter, Listen, Host } from '@stencil/core';

@Component({
	tag: 'lsg-accordion-element',
	styleUrl: 'lsg-accordion-element.css',
	shadow: true,
})
export class LsgAccordionElement implements ComponentInterface {

	@Prop() headertext: string;
	@Prop({reflect: true}) isOpen: boolean = false;

	@Event() toggle: EventEmitter<boolean>;

	@Listen('click')
	handleClick(){
		console.timeStamp("click");
		this.toggle.emit(this.isOpen);
	}

	render() {
		return (
			<Host>
				<lsg-accordion-header
					headertext={this.headertext}
					isOpen={this.isOpen}>
				</lsg-accordion-header>
				<lsg-accordion-panel
					class={{'collapsed': !this.isOpen}}
					id="panel">
					<slot></slot>
				</lsg-accordion-panel>
				<hr class={{'open': this.isOpen}}/>
			</Host>
		);
	}
}
