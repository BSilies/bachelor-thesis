# lsg-accordion-element



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute    | Description | Type      | Default     |
| ------------ | ------------ | ----------- | --------- | ----------- |
| `headertext` | `headertext` |             | `string`  | `undefined` |
| `isOpen`     | `is-open`    |             | `boolean` | `false`     |


## Events

| Event    | Description | Type                   |
| -------- | ----------- | ---------------------- |
| `toggle` |             | `CustomEvent<boolean>` |


## Dependencies

### Depends on

- [lsg-accordion-header](../lsg-accordion-header)
- [lsg-accordion-panel](../lsg-accordion-panel)

### Graph
```mermaid
graph TD;
  lsg-accordion-element --> lsg-accordion-header
  lsg-accordion-element --> lsg-accordion-panel
  lsg-accordion-header --> lsg-basic-headline
  lsg-accordion-header --> lsg-basic-icon
  style lsg-accordion-element fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
