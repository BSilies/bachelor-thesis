# lsg-accordion-header



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute    | Description | Type      | Default     |
| ------------ | ------------ | ----------- | --------- | ----------- |
| `headertext` | `headertext` |             | `string`  | `undefined` |
| `isOpen`     | `is-open`    |             | `boolean` | `false`     |


## Dependencies

### Used by

 - [lsg-accordion-element](../lsg-accordion-element)

### Depends on

- [lsg-basic-headline](../lsg-basic-headline)
- [lsg-basic-icon](../lsg-basic-icon)

### Graph
```mermaid
graph TD;
  lsg-accordion-header --> lsg-basic-headline
  lsg-accordion-header --> lsg-basic-icon
  lsg-accordion-element --> lsg-accordion-header
  style lsg-accordion-header fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
