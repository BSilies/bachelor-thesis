import { Component, ComponentInterface, h, Prop } from '@stencil/core';
import { Icons, Size } from '../lsg-basic-icon/lsg-basic-icon-settings';

@Component({
	tag: 'lsg-accordion-header',
	styleUrl: 'lsg-accordion-header.css',
	shadow: true,
})
export class LsgAccordionHeader implements ComponentInterface {

	@Prop() headertext: string;
	@Prop() isOpen: boolean = false;

	render() {
		return (
			<div class={{
				'lsg-accordion-header-container': true,
				'open': this.isOpen
			}}>
				<lsg-basic-headline
					content={this.headertext}>
				</lsg-basic-headline>
				<lsg-basic-icon
					class={{'open': this.isOpen}}
					iconname={Icons.CHEVRON_DOWN}
					size={Size.SMALL}>
				</lsg-basic-icon>
			</div>
		);
	}
}
