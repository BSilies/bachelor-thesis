# lsg-basic-headline



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type     | Default     |
| --------- | --------- | ----------- | -------- | ----------- |
| `content` | `content` |             | `string` | `undefined` |


## Dependencies

### Used by

 - [lsg-accordion-header](../lsg-accordion-header)

### Graph
```mermaid
graph TD;
  lsg-accordion-header --> lsg-basic-headline
  style lsg-basic-headline fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
