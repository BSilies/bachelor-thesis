import { Component, ComponentInterface, h, Prop } from '@stencil/core';

@Component({
	tag: 'lsg-basic-headline',
	styleUrl: 'lsg-basic-headline.css',
	shadow: true,
})
export class LsgBasicHeadline implements ComponentInterface {

	@Prop() content: string

	render() {
		return (
			<h3>{this.content}</h3>
		);
	}
}
