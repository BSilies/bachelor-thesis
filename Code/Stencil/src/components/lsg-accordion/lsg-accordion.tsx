import { Component, ComponentInterface, h, Prop, Watch, Element, Listen, State } from '@stencil/core';
import { Mode } from './lsg-accordion-settings';

@Component({
	tag: 'lsg-accordion',
	styleUrl: 'lsg-accordion.css',
	shadow: true,
})
export class LsgAccordion implements ComponentInterface {

	@Prop() mode: Mode = Mode.MULTIPLE;
	@State() children: any = [];
	@Element() host: HTMLElement;

	observer: MutationObserver

	connectedCallback() {
		this.observer = new MutationObserver(() => this.registerChildren());
		this.registerChildren();
		this.observer.observe(this.host, {childList: true})
	}

	disconnectedCallback() {
		this.observer.disconnect();
	}

	componentWillLoad(){
		this.checkMode();
	}

	registerChildren() {
		this.children = [];
		for (let i = 0; i < this.host.children.length; i++) {
			this.children.push(this.host.children.item(i));
		}
		this.checkMode();
	}

	@Watch('mode')
	checkMode() {
		console.timeStamp("setMode");
		if (this.mode != Mode.MULTIPLE && this.children.length > 0) {
			let openChildren = this.children.filter(child => child.isOpen);
			if (openChildren.length > 1) {
				openChildren.shift();
				openChildren.forEach(child => child.isOpen = false);
			} else if (openChildren.length == 0 && this.mode == Mode.STRICT) {
				this.children[0].isOpen = true;
			}
		}
		console.timeStamp("checkModeEnd");
	}

	@Listen('toggle')
	handleToggle(e) {
		switch (this.mode) {
			case Mode.STRICT:
				if (!e.srcElement.isOpen) {
					try {
						let openChild = this.children.find(child => child.isOpen);
						openChild.isOpen = false;
					} catch { ; }
					e.srcElement.isOpen = true;
				}
				break;
			case Mode.SINGLE:
				if (!e.srcElement.isOpen) {
					try {
						let openChild = this.children.find(child => child.isOpen);
						openChild.isOpen = false;
					} catch { ; }
					e.srcElement.isOpen = true;
				} else {
					e.srcElement.isOpen = false;
				}
				break;
			default:
				e.srcElement.isOpen = !e.srcElement.isOpen;
				break;
		}
		console.timeStamp("handleToggleEnd");
	}

	render() {
		return (
			<slot></slot>
		);
	}
}
