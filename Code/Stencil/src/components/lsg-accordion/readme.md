# lsg-accordion



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type                                          | Default         |
| -------- | --------- | ----------- | --------------------------------------------- | --------------- |
| `mode`   | `mode`    |             | `Mode.MULTIPLE \| Mode.SINGLE \| Mode.STRICT` | `Mode.MULTIPLE` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
