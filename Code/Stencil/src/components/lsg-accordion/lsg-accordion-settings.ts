export enum Mode {
	MULTIPLE = 'multiple',
	SINGLE = 'single',
	STRICT = 'strict'
}
