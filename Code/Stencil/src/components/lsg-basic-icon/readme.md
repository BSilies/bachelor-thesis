# lsg-basic-icon



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description | Type                                      | Default              |
| ---------- | ---------- | ----------- | ----------------------------------------- | -------------------- |
| `iconname` | `iconname` |             | `Icons.ARROW_DOWN \| Icons.CHEVRON_DOWN`  | `Icons.CHEVRON_DOWN` |
| `isSolid`  | `is-solid` |             | `boolean`                                 | `false`              |
| `size`     | `size`     |             | `Size.LARGE \| Size.MEDIUM \| Size.SMALL` | `Size.SMALL`         |


## Dependencies

### Used by

 - [lsg-accordion-header](../lsg-accordion-header)

### Graph
```mermaid
graph TD;
  lsg-accordion-header --> lsg-basic-icon
  style lsg-basic-icon fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
