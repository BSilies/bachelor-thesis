import { Component, ComponentInterface, h, Prop, getAssetPath } from '@stencil/core';
import { Icons, Size } from './lsg-basic-icon-settings';

@Component({
	tag: 'lsg-basic-icon',
	styleUrl: 'lsg-basic-icon.css',
	shadow: true,
	assetsDirs: ['Icons']
})
export class LsgBasicIcon implements ComponentInterface {

	@Prop() iconname: Icons = Icons.CHEVRON_DOWN;
	@Prop() size: Size = Size.SMALL;
	@Prop() isSolid: boolean = false;

	render() {
		return (
			<img alt="arrow down" src={getAssetPath(`./Icons/${this.iconname}_${this.size}_${this.isSolid ? 'solid': 'outline'}.svg`)}></img>
		);
	}
}
