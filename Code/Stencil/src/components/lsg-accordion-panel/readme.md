# lsg-accordion-panel



<!-- Auto Generated Below -->


## Dependencies

### Used by

 - [lsg-accordion-element](../lsg-accordion-element)

### Graph
```mermaid
graph TD;
  lsg-accordion-element --> lsg-accordion-panel
  style lsg-accordion-panel fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
