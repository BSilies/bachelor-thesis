import { Component, ComponentInterface, h } from '@stencil/core';

@Component({
	tag: 'lsg-accordion-panel',
	styleUrl: 'lsg-accordion-panel.css',
	shadow: true,
})
export class LsgAccordionPanel implements ComponentInterface {

	render() {
		return (
			<slot></slot>
		);
	}
}
