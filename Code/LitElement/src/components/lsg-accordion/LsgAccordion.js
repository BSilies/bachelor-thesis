import { LitElement, html, css } from "lit-element";
import { Mode } from "../../../../Common/LsgAccordionSettings";

export class LsgAccordion extends LitElement{

	static get properties() {return{
		mode: {type: String},
	};}

	constructor(){
		super();
		this.mode = Mode.MULTIPLE;
		this.addEventListener('toggle', this.handleToggle);
		this.observer = new MutationObserver(() => this.registerChildren());
	}

	connectedCallback() {
		super.connectedCallback();
		this.registerChildren();
		this.observer.observe(this, {childList: true})
	}

	disconnectedCallback() {
		super.disconnectedCallback();
		this.observer.disconnect();
	}

	updated(changedProperties){
		console.timeStamp("setMode");
		this.checkMode();
	}

	registerChildren() {
		this._children = [];
		for (let i = 0; i < this.children.length; i++) {
			this._children.push(this.children.item(i));
		}
		this.checkMode();
	}

	checkMode() {
		if (this.mode != Mode.MULTIPLE && this._children.length > 0) {
			let openChildren = this._children.filter(child => child.isOpen);
			if (openChildren.length > 1) {
				openChildren.shift();
				openChildren.forEach(child => child.isOpen = false);
			} else if (openChildren.length == 0 && this.mode == Mode.STRICT) {
				this._children[0].isOpen = true;
			}
		}
		console.timeStamp("checkModeEnd");
	}

	handleToggle(e) {
		switch (this.mode) {
			case Mode.STRICT:
				if (!e.srcElement.isOpen) {
					try {
						let openChild = this._children.find(child => child.isOpen);
						openChild.isOpen = false;
					} catch { ; }
					e.srcElement.isOpen = true;
				}
				break;
			case Mode.SINGLE:
				if (!e.srcElement.isOpen) {
					try {
						let openChild = this._children.find(child => child.isOpen);
						openChild.isOpen = false;
					} catch { ; }
					e.srcElement.isOpen = true;
				} else {
					e.srcElement.isOpen = false;
				}
				break;
			default:
				e.srcElement.isOpen = !e.srcElement.isOpen;
				break;
		}
		console.timeStamp("handleToggleEnd");
	}

	static get styles(){
		return css`
			::slotted(lsg-accordion-element:last-child){
				--display-hr: none;
			}

			:host{
				margin: 32px 0;
				display: block;
				--color-ending-line: #eaeaea;
				--color-headline-background: #ffffff;
				--color-headline-background-hover: #f4f4f4;
				--color-headline-text: #000000;
				--color-icon: #000000;
			}
		`
	}

	render(){
		return html`
			<slot></slot>
		`
	}

}
customElements.define('lsg-accordion', LsgAccordion)
