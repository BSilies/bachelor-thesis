import { LitElement, html, css } from "lit-element";
import { classMap } from 'lit-html/directives/class-map';
export * from '../lsg-accordion-header/LsgAccordionHeader.js'
export * from '../lsg-accordion-panel/LsgAccordionPanel.js'

export class LsgAccordionElement extends LitElement {

	static get properties() {return{
		headertext: {type: String},
		isOpen: {type: Boolean, attribute: 'is-open', reflect: true}
	};}

	constructor(){
		super();
		this.headertext = '';
		this.isOpen = false;
	}

	handleClick(e){
		console.timeStamp("click");
		let event = new CustomEvent('toggle', {
			detail:{
				isOpen: this.isOpen
			},
			bubbles: true,
			composed: true
		});
		this.dispatchEvent(event);
	}

	static get styles(){
		return css`
			hr {
				height: 1px;
				border: none;
				background-color: var(--color-ending-line, #eaeaea);
				margin: 0 16px;
				display: var(--display-hr, block);
			}

			hr.open {
				margin-top: 16px;
			}

			lsg-accordion-panel{
				display: block;
				padding-left: 32px;
				padding-right: 16px;
				height: auto;
				transition: height 2s ease-out;
			}

			lsg-accordion-panel.collapsed {
				display: none;
				overflow: hidden;
				height: 0;
			}

			::slotted(*){
				margin-left: 0;
				margin-right: 0;
				font-family: verdana, sans-serif;
			}
		`
	}

	render(){
		return html`
			<lsg-accordion-header
				.headertext="${this.headertext}"
				.isOpen="${this.isOpen}"
				@click="${this.handleClick}">
			</lsg-accordion-header>
			<lsg-accordion-panel
				class="${classMap({collapsed: !this.isOpen})}"
				id="panel">
				<slot></slot>
			</lsg-accordion-panel>
			<hr class="${classMap({open: this.isOpen})}">
		`
	}

}
customElements.define('lsg-accordion-element', LsgAccordionElement);
