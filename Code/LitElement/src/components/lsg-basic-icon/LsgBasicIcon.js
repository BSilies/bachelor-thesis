import { LitElement, html, css } from "lit-element";
import { Size, Icons } from "../../../../Common/LsgBasicIconSettings";

export class LsgBasicIcon extends LitElement{

	static get properties() { return {
		iconname: { type: String },
		icon: {type: String},
		size: {type: Number},
		isSolid: {type: Boolean, attribute: 'is-solid'}
	};}

	constructor() {
		super();
		this.iconname = Icons.CHEVRON_DOWN;
		this.size = Size.SMALL;
		this.isSolid = false;
	}

	updated(changedProperties){
		let appearance;
		if(this.isSolid) {
			appearance = 'solid';
		} else {
			appearance = 'outline';
		}
		import(`../../../../Common/Icons/${this.iconname}_${this.size}_${appearance}.svg`)
			.then(loadedIcon => {
				this.icon = loadedIcon.default;
			});
	}

	static get styles(){
		return css`
			img {
				display: block;
			}
		`
	}

	render() {
		return html`
			<img alt="arrow down" src=${this.icon}>
		`
	}
}
customElements.define('lsg-basic-icon', LsgBasicIcon)
