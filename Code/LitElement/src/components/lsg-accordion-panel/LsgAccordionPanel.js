import { LitElement, html, css } from "lit-element";

export class LsgAccordionPanel extends LitElement {

	constructor(){
		super();
	}

	render(){
		return html`
			<slot></slot>
		`
	}

}
customElements.define('lsg-accordion-panel', LsgAccordionPanel);
