import { LitElement, html, css } from "lit-element";

export class LsgBasicHeadline extends LitElement{

	static get properties() { return {
		content: { type: String },
	};}

	constructor() {
		super();
		this.content = '';
	}

	static get styles() {
		return css`
			h3 {
				font-family: “Arial Bold”, sans-serif;
				float: inherit;
				margin: 0;
			}
		`;
	}

	render() {
		return html`
			<h3>${this.content}</h3>
		`
	}
}
customElements.define('lsg-basic-headline', LsgBasicHeadline)
