module.exports = {
    presets: [
        [ "@babel/preset-env", {
            "useBuiltIns": "usage",
            "bugfixes": true,
			"debug": true,
			"corejs": 3
		}]
    ],
    plugins: [
        '@babel/plugin-proposal-class-properties',
        [
            '@babel/plugin-transform-runtime',
            {
                regenerator: true,
            },
        ],
    ],
};
