export const Mode = {
	MULTIPLE: 'multiple',
	SINGLE: 'single',
	STRICT: 'strict'
};
