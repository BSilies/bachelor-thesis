export const Size = {
	SMALL: 24,
	MEDIUM: 48,
	LARGE: 96,
};

export const Icons = {
	ARROW_DOWN: 'interaction_arrows_arrow-down',
	CHEVRON_DOWN: 'interaction_arrows_chevron-down'
};
