import { Mode } from "../../../../Common/LsgAccordionSettings";

const template = document.createElement('template');
template.innerHTML = `
	<style>
		::slotted(lsg-accordion-element:last-child){
			--display-hr: none;
		}

		:host{
			margin: 32px 0;
			display: block;
			--color-ending-line: #eaeaea;
			--color-headline-background: #ffffff;
			--color-headline-background-hover: #f4f4f4;
			--color-headline-text: #000000;
			--color-icon: #000000;
		}
	</style>
	<slot></slot>
`;

export class LsgAccordion extends HTMLElement {

	constructor() {
		super();
		this._shadowRoot = this.attachShadow({ 'mode': 'open' });
		this._shadowRoot.appendChild(template.content.cloneNode(true));

		this.addEventListener('toggle', this.handleToggle);

		this._mode = Mode.MULTIPLE;
		this._children = [];

		this.observer = new MutationObserver(() => this.registerChildren());
	}

	static get observedAttributes() {
		return ['mode'];
	}

	attributeChangedCallback(name, oldVal, newVal) {
		this.mode = newVal;
	}

	connectedCallback() {
		if (this.hasOwnProperty("mode")) {
			let value = this["mode"];
			delete this["mode"];
			this["mode"] = value;
		}
		this.registerChildren();
		this.observer.observe(this, {childList: true})
	}

	disconnectedCallback() {
		this.observer.disconnect();
	}

	set mode(val) {
		console.timeStamp("setMode");
		this._mode = val;
		this.checkMode();
	}

	get mode() {
		return this._mode;
	}

	registerChildren() {
		this._children = [];
		for (let i = 0; i < this.children.length; i++) {
			this._children.push(this.children.item(i));
		}
		this.checkMode();
	}

	checkMode() {
		if (this.mode != Mode.MULTIPLE && this._children.length > 0) {
			let openChildren = this._children.filter(child => child.isOpen);
			if (openChildren.length > 1) {
				openChildren.shift();
				openChildren.forEach(child => child.isOpen = false);
			} else if (openChildren.length == 0 && this.mode == Mode.STRICT) {
				this._children[0].isOpen = true;
			}
		}
		console.timeStamp("checkModeEnd");
	}

	handleToggle(e) {
		switch (this.mode) {
			case Mode.STRICT:
				if (!e.srcElement.isOpen) {
					try {
						let openChild = this._children.find(child => child.isOpen);
						openChild.isOpen = false;
					} catch { ; }
					e.srcElement.isOpen = true;
				}
				break;
			case Mode.SINGLE:
				if (!e.srcElement.isOpen) {
					try {
						let openChild = this._children.find(child => child.isOpen);
						openChild.isOpen = false;
					} catch { ; }
					e.srcElement.isOpen = true;
				} else {
					e.srcElement.isOpen = false;
				}
				break;
			default:
				e.srcElement.isOpen = !e.srcElement.isOpen;
				break;
		}
		console.timeStamp("handleToggleEnd");
	}

}
customElements.define('lsg-accordion', LsgAccordion)
