import { Icons, Size } from "../../../../Common/LsgBasicIconSettings";
export * from '../lsg-basic-headline/LsgBasicHeadline.js'
export * from '../lsg-basic-icon/LsgBasicIcon.js'

const template = document.createElement('template');
template.innerHTML = `
	<style>
		lsg-basic-headline {
			float: left;
			display: block;
			margin: 28px 0;
		}

		lsg-basic-icon {
			position: absolute;
			float: left;
			top: 50%;
			right: 0;
			margin-top: -12px;
			margin-right:32px;
			margin-left:16px;
			transition: transform 0.2s;
		}

		lsg-basic-icon.open {
			-ms-transform: rotate(180deg);
			transform: rotate(180deg);
		}

		#lsg-accordion-header-container {
			overflow: auto;
			position: relative;
			cursor: default;
			padding-left: 16px;
			padding-right: 72px;
			transition: padding 0.2s;
			background-color: var(--color-headline-background, #ffffff)
		}

		#lsg-accordion-header-container:hover {
			background-color: var(--color-headline-background-hover, #f4f4f4)
		}

		#lsg-accordion-header-container:hover, #lsg-accordion-header-container.open {
			padding-left: 32px;
		}
	</style>
	<div id="lsg-accordion-header-container">
		<lsg-basic-headline id="headline"></lsg-basic-headline>
		<lsg-basic-icon
			id="icon"
			iconname="${Icons.CHEVRON_DOWN}"
			size="${Size.SMALL}">
		</lsg-basic-icon>
	</div>
`;

export class LsgAccordionHeader extends HTMLElement{

	constructor() {
		super();
		this._shadowRoot = this.attachShadow({ 'mode': 'open' });
		this._shadowRoot.appendChild(template.content.cloneNode(true));
		this.headline = this._shadowRoot.querySelector('#headline');
		this.icon = this._shadowRoot.querySelector('#icon');
		this.wrapper = this._shadowRoot.querySelector('#lsg-accordion-header-container');

		this._headertext = '';
		this._isOpen = false;
	}

	set isOpen(val) {
		this._isOpen = val;
		this.render();
	}

	get isOpen() {
		return this._isOpen;
	}

	set headertext(val) {
		this._headertext = val;
		this.render();
	}

	get headertext() {
		return this._headertext;
	}

	static get observedAttributes() {
		return ['headertext', 'is-open'];
	}

	connectedCallback() {
		this._upgradeProperty('isOpen');
		this._upgradeProperty('headertext');
		this.render();
	  }


	attributeChangedCallback(name, oldVal, newVal) {
		switch (name) {
			case 'headertext':
				this.headertext = newVal;
				break;
			case 'is-open':
				this.isOpen = newVal;
				break;
		}
	}

	_upgradeProperty(prop) {
		if (this.hasOwnProperty(prop)) {
			let value = this[prop];
			delete this[prop];
			this[prop] = value;
		}
	}

	render() {
		this.headline.content=this.headertext;
		if(this.isOpen){
			this.wrapper.classList.add(['open']);
			this.icon.classList.add(['open']);
		} else {
			this.wrapper.classList.remove(['open']);
			this.icon.classList.remove(['open']);
		}
	}
}
customElements.define('lsg-accordion-header', LsgAccordionHeader);
