export * from '../lsg-accordion-header/LsgAccordionHeader.js'
export * from '../lsg-accordion-panel/LsgAccordionPanel.js'

const template = document.createElement('template');
template.innerHTML = `
	<style>
		hr {
			height: 1px;
			border: none;
			background-color: var(--color-ending-line, #eaeaea);
			margin: 0 16px;
			display: var(--display-hr, block);
		}

		hr.open {
			margin-top: 16px;
		}

		lsg-accordion-panel{
			display: block;
			padding-left: 32px;
			padding-right: 16px;
			height: auto;
			transition: height 2s ease-out;
		}

		lsg-accordion-panel.collapsed {
			display: none;
			overflow: hidden;
			height: 0;
		}

		::slotted(*){
			margin-left: 0;
			margin-right: 0;
			font-family: verdana, sans-serif;
		}
	</style>
	<lsg-accordion-header>
	</lsg-accordion-header>
	<lsg-accordion-panel id="panel">
		<slot></slot>
	</lsg-accordion-panel>
	<hr/>
`;

export class LsgAccordionElement extends HTMLElement {

	constructor(){
		super();
		this._shadowRoot = this.attachShadow({ 'mode': 'open' });
		this._shadowRoot.appendChild(template.content.cloneNode(true));
		this.seperator = this._shadowRoot.querySelector('hr');
		this.panel = this._shadowRoot.querySelector('lsg-accordion-panel');
		this.header = this._shadowRoot.querySelector('lsg-accordion-header');

		this.header.addEventListener('click', this.handleClick);

		this._headertext = '';
	}

	set isOpen(val) {
		if(val){
			this.setAttribute("is-open", "");
		} else {
			this.removeAttribute("is-open");
		}
	}

	get isOpen() {
		return this.hasAttribute("is-open");
	}

	set headertext(val) {
		this._headertext = val;
		this.render();
	}

	get headertext() {
		return this._headertext;
	}

	static get observedAttributes() {
		return ['headertext', "is-open"];
	}

	connectedCallback(){
		this._upgradeProperty('isOpen');
		this._upgradeProperty('headertext');
		this.render();
	}

	_upgradeProperty(prop) {
		if (this.hasOwnProperty(prop)) {
			let value = this[prop];
			delete this[prop];
			this[prop] = value;
		}
	}

	attributeChangedCallback(name, oldVal, newVal) {
		switch (name) {
			case 'headertext':
				this.headertext = newVal;
				break;
			case 'is-open':
				this.render();
				break;
		}
    }

	handleClick(e){
		console.timeStamp("click");
		let event = new CustomEvent('toggle', {
			detail:{
				isOpen: this.isOpen
			},
			bubbles: true,
			composed: true
		});
		this.dispatchEvent(event);
	}

	render(){
		this.header.isOpen = this.isOpen;
		this.header.headertext = this.headertext;
		if(this.isOpen){
			this.seperator.classList.add(['open']);
			this.panel.classList.remove(['collapsed'])
		} else {
			this.seperator.classList.remove(['open']);
			this.panel.classList.add(['collapsed'])
		}
	}

}
customElements.define('lsg-accordion-element', LsgAccordionElement);
