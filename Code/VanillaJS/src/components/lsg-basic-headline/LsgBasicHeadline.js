const template = document.createElement('template');
template.innerHTML = `
	<style>
		h3 {
			font-family: “Arial Bold”, sans-serif;
			float: inherit;
			margin: 0;
		}
	</style>
	<h3 id="headline"></h3>
`;

export class LsgBasicHeadline extends HTMLElement{

	constructor() {
		super();
		this._shadowRoot = this.attachShadow({ 'mode': 'open' });
		this._shadowRoot.appendChild(template.content.cloneNode(true));
		this.headline = this._shadowRoot.querySelector('#headline');

		this._content='';
	}

	static get observedAttributes() {
        return ['content'];
	}

	set content (val){
		this._content = val;
		this.render();
	}

	get content (){
		return this._content;
	}

	connectedCallback() {
		if (this.hasOwnProperty("content")) {
			let value = this["content"];
			delete this["content"];
			this["content"] = value;
		}
        this.render();
    }

	attributeChangedCallback(name, oldVal, newVal) {
		this.content = newVal;
    }

	render() {
		this.headline.innerHTML=this.content;
	}
}
customElements.define('lsg-basic-headline', LsgBasicHeadline)
