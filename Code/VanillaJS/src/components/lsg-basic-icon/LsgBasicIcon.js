import { Size, Icons } from "../../../../Common/LsgBasicIconSettings";

const template = document.createElement('template');
template.innerHTML = `
	<style>
		img {
			display: block;
		}
	</style>
	<img id="icon" alt="arrow down">
`;

export class LsgBasicIcon extends HTMLElement{

	constructor() {
		super();
		this._shadowRoot = this.attachShadow({ 'mode': 'open' });
		this._shadowRoot.appendChild(template.content.cloneNode(true));
		this.icon = this._shadowRoot.querySelector('#icon');

		this._iconname = Icons.CHEVRON_DOWN;
		this._size = Size.SMALL;
		this._isSolid = false;
	}

	get iconname() {
		return this._iconname;
	}

	set iconname(val) {
		this._iconname = val;
		this.loadIcon(this._iconname, this.size, this.isSolid);
	}

	get size() {
		return this._size;
	}

	set size(val) {
		this._size = val;
		this.loadIcon(this.iconname, this._size, this.isSolid);
	}

	get isSolid() {
		return this._isSolid;
	}

	set isSolid(val) {
		this._isSolid = val;
		this.loadIcon(this.iconname, this.size, this._isSolid);
	}

	static get observedAttributes() {
		return ['iconname', 'size', 'is-solid'];
	}

	connectedCallback() {
		this._upgradeProperty('iconname');
		this._upgradeProperty('size');
		this._upgradeProperty('isSolid');
		this.loadIcon(this.iconname, this.size, this.isSolid);
    }

	attributeChangedCallback(name, oldVal, newVal) {
		switch (name) {
			case 'iconname':
				this.iconname = newVal;
				break;
			case 'size':
				this.size = parseInt(newVal);
				break;
			case 'is-solid':
				this.isSolid = newVal !== null;
				break;
			default:
				break;
		}
	}

	_upgradeProperty(prop) {
		if (this.hasOwnProperty(prop)) {
			let value = this[prop];
			delete this[prop];
			this[prop] = value;
		}
	}

	loadIcon(iconname, size, isSolid){
		let appearance;
		if(isSolid) {
			appearance = 'solid';
		} else {
			appearance = 'outline';
		}
		import(`../../../../Common/Icons/${iconname}_${size}_${appearance}.svg`)
			.then(loadedIcon => {
				this.iconFile = loadedIcon.default;
				this.render();
			});
	}

	render() {
		this.icon.setAttribute("src", this.iconFile);
	}
}
customElements.define('lsg-basic-icon', LsgBasicIcon)
