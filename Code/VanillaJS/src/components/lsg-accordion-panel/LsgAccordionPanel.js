const template = document.createElement('template');
template.innerHTML = `
	<slot></slot>
`;

export class LsgAccordionPanel extends HTMLElement {

	constructor(){
		super();
		this._shadowRoot = this.attachShadow({ 'mode': 'open' });
		this._shadowRoot.appendChild(template.content.cloneNode(true));
	}

}
customElements.define('lsg-accordion-panel', LsgAccordionPanel);
